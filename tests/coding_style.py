#!/usr/bin/python
# -*- coding: UTF-8 -*-

################################### WELCOME ###################################
# Kevin AVOGNON
# Promo EPITA 2017
################################## !WELCOME ###################################

import linecache
import glob
import sys
import os

################################ COLOR DEFINITION #############################
class color:
  RED     = '\033[91m'
  GREEN   = '\033[92m'
  YELLOW  = '\033[93m'
  BLUE    = '\033[94m'
  PINK    = '\033[95m'
  CYAN    = '\033[96m'
  RESET   = '\033[0m'

###################################   eader ####################################
print color.RESET
print "\t\t--->      EPITA C/C++ Coding style checker\t<---"
print "\t\t--->  Made by Kevin \'lintel\' AVOGNON (avogno_k)\t<---"
print "\t\t--->     Using Python 2.7.8 on Ubuntu 14.10\t<---\n"
print

############################### FUNCTIONS #####################################
class ccppfile:
    err = 0
    found = 0
    listerr = []

def filetreat(cppfile):
  if (is_c_or_cpp(cppfile)):
    ccppfile.found += 1
    ccppfile.err = 0
    #print color.RED
    filefunc = open(cppfile, "r")
    buf = filefunc.read();
    #print color.BLUE + "Checking File " + color.YELLOW + cppfile + color.BLUE
    cppfile = cppfile.replace("/home/kev", '~')
    namecase(cppfile)
    Check_Coding_Style(cppfile)
    filefunc.close()

def namecase(filename):
  i = 0
  filename = filename.replace("~/", '')
  while (filename[i] != '.'):
    if (filename[i] == '/'):
      return;
    if ((filename[i] < 'a' or filename[i] > 'z') and filename[i] != '_'):
      print color.RED + "Wrong file name: " + color.YELLOW + filename\
          + color.RED + " Asked :[a-z][a-z0-9_]* "
      ccppfile.err += 1
    i += 1

def end(c):
  if (c == '\n' or c == '\0' or c == ''):
    return True
  else:
    return False

def bounds(line, char):
  b = -1
  e = -1
  if (char == '('):
    b = 0
    e = 0
    while (line[b] != '(' and end(line[b]) == False):
      b += 1
      e +=1
      if (end(line[b])):
        return [-1, -1]
    while (line[e] != ')' and end(line[e]) == False):
      e +=1
      if (end(line[e])):
        return [b, -1]
  if (char == '\"'):
    b = 0
    e = 0
    while (line[b] != '\"' and end(line[b]) == False):
      b += 1
      e +=1
      if (end(line[b])):
        return [-1, -1]
    e +=1
    while (line[e] != '\"' and end(line[e]) == False):
      e +=1
      if (end(line[e])):
        return [b, -1]
  if (char == '\''):
    b = 0
    e = 0
    while (line[b] != '\'' and end(line[b]) == False):
      b += 1
      e +=1
      if (end(line[b])):
        return [-1, -1]
    e +=1
    while (line[e] != '\'' and end(line[e]) == False):
      e +=1
      if (end(line[e])):
        return [b, -1]
  return [b, e]


def is_in(line, bound, char):
  if (line.find(bound) == -1):
    return False
  [b, e] = bounds(line, bound)
  if (line.find(char) > b and line.find(char) <= e):
    return True
  else:
    return False


def is_good(line):
  if (is_in(line, "\"", '-') or
      is_in(line, "\"", '+') or
      is_in(line, "\"", '%') or
      is_in(line, "\"", '=') or
      is_in(line, "\"", '/')):
    return False
  else:
    return True


def isnum(c):
  if (c == '0' or c == '1' or\
      c == '2' or c == '3' or\
      c == '4' or c == '5' or\
      c == '6' or c == '7' or\
      c == '8' or c == '9'):
    return True
  else:
    return False


def get_line(filename):
  nblines = 1
  lines = []
  while (linecache.getline(filename, nblines) != ""):
    lines.append(linecache.getline(filename, nblines))
    nblines += 1
  return [nblines, lines]

def My_strcmp(inp, expl):
  i = 0
  if (len(inp) < len(expl)):
    return False
  while (i < len(expl) and inp[i] == expl[i]):
    i += 1
  if (i == len(expl)):
    return True
  else:
    return False


def Count80col(filename, line, i):
  j = 0
  while (line[j] != "\n" and line[j] != ""):
    j += 1
  if (line[j - 1] == " "):
    ccppfile.err += 1
    print color.RED + "Blank space left " + color.YELLOW + filename + ": l."\
        + str(i)
  if (j >= 80):
    print color.RED + "Code goin over 80 cols " + color.YELLOW + filename \
        + ": l." + str(i)
    ccppfile.err += 1


def keywords(filename, token, line):
  if (token.find("fork") == -1 and token.find("return;") == -1    and
     ((My_strcmp(token, "if")     and len(token) > len("if"))     or
      (My_strcmp(token, "while")  and len(token) > len("while"))  or
      (My_strcmp(token, "switch") and len(token) > len("switch")) or
      (My_strcmp(token, "return") and len(token) > len("return")) or
      (My_strcmp(token, "for")    and len(token) > len("for")))):
    print color.RED + "Keyword needs to be followed by a whitespace "\
          + color.YELLOW + filename + ": l." + str(line)
    print color.RESET + token
    ccppfile.err += 1
  if (token == "goto"):
    print color.RED + "The keyword \"goto\" must not be used "\
        + color.YELLOW + filename + ":l." + str(line)
    ccppfile.err += 1


def punctuation(filename, line, token, i):
  if (token == ';' and len(line) == 1):
    print color.RED + "Semicolon should not be alone on his line "\
        + color.YELLOW + filename + ":l." + str(i)
    print color.RESET + token
    ccppfile.err += 1
  if (token.find(',') != -1 and token.find(',') != len(token) - 1):
    print color.RED + "Comma needs to be followed by a whitespace "\
        + color.YELLOW + filename + ": l." + str(i)
    print color.RESET + token
    ccppfile.err += 1
  if (line.count(';') >= 2 and\
      token.find(';') != -1 and
      token.find(';') != len(token) - 1 and
       (end(token[token.find(';') + 1]) != True and\
           token[token.find(';') + 1] != ' ')):
    print color.RED + "Semicolon must be followed by a whitespace "\
          + color.YELLOW + filename + ":l." + str(i)
    print color.RESET + token
    ccppfile.err += 1


def operators(filename, token, i):
  if ((token.find("//") == -1) and
      (token.find("/*") == -1) and
      (token.find("**") == -1) and
      (token.find("*/") == -1) and
      (token.find("<<") == -1) and
      (token.find(">>") == -1) and
      (token.find("operator") == -1) and
      (token.find("std") == -1) and
      (token.find("class") == -1) and
      (token.find("::") == -1) and
      (token.find("int") == -1) and
      (token.find("char") == -1) and
      (token.find("->") == -1) and
      (token.find("--") == -1) and
      (token.find("++") == -1)):
    if ((token.find(">=") != -1 and len(token) != 2) or
        (token.find("<=") != -1 and len(token) != 2) or
        (token.find("<<") != -1 and len(token) != 2) or
        (token.find(">>") != -1 and len(token) != 2) or
        (token.find("!=") != -1 and len(token) != 2) or
        (token.find("==") != -1 and len(token) != 2) or
        (token.find("%=") != -1 and len(token) != 2) or
        (token.find("/=") != -1 and len(token) != 2) or
        (token.find("*=") != -1 and len(token) != 2) or
        (token.find("-=") != -1 and len(token) != 2) or
        (token.find("&&") != -1 and len(token) != 2) or
        (token.find("||") != -1 and len(token) != 2) or
        (token.find("+=") != -1 and len(token) != 2)):
      ccppfile.err += 1
      print color.RED + "Operators must be padded by spaces on both sides "\
        + color.YELLOW + filename + ":l." + str(i)
      print color.RESET + token
    if (token != "==" and\
        (token.find("/") != -1 and token.find('/') < len(token) - 1 and\
        token[token.find('/') + 1] != '=') or\
        (token.find("<") != -1 and token.find('<') < len(token) - 1 and\
        token[token.find('<') + 1] != '=') or\
        (token.find(">") != -1 and token.find('>') < len(token) - 1 and\
        token[token.find('>') + 1] != '=') or\
        (token.find("%") != -1 and token.find('%') < len(token) - 1 and\
        token[token.find('%') + 1] != '=') or\
        (token.find("*") != -1 and token.find('*') < len(token) - 1 and\
        token[token.find('*') + 1] != '=') or\
        (token.find("-") != -1 and token.find('-') < len(token) - 1 and\
        token[token.find('-') + 1] != '=') or\
        (token.find("+") != -1 and token.find('+') < len(token) - 1 and\
        token[token.find('+') + 1] != '=')):
      m = token.find('*')
      if (m != -1 or isnum(token[m + 1] == False)):
        return;
      if (is_good(token)):
        return;
      ccppfile.err += 1
      print color.RED + "Operators must be padded by spaces on both sides "\
          + color.YELLOW + filename + ":l." + str(i)
      print color.RESET + token


def linebreak(filename, line, i):
  if (line.find("&&") != -1 and line[len(line) - 2] == '&'):
    ccppfile.err += 1
    print color.RED + "Operator \"&&\" should be at the beginning of a line "\
        + color.YELLOW + filename + ":l." + str(i)
    line.replace('\n', '')
    print color.RESET + line.replace('  ', '')
  if (line.find("||") != -1 and line[len(line) - 2] == '|'):
    ccppfile.err += 1
    print color.RED + "Operator \"||\" should be at the beginning of a line "\
        + color.YELLOW + filename + ":l." + str(i)
    line.replace('\n', '')
    print color.RESET + line.replace('  ', '')

def EvalLine(filename, line, i):
  tokens = line.split()
  linebreak(filename, line, i)
  if (line.find(';') != -1 and line[line.find(';') - 1] == ' '\
      and len(tokens) > 1):
    print color.RED + "Semicolon must not be preceded by a whitespace "\
        + color.YELLOW + filename + ":l." + str(i)
    ccppfile.err += 1
  for token in tokens:
    keywords(filename, token, i)
    punctuation(filename, line, token, i)
    if (is_good(line)):
      operators(filename, token, i)
    if (is_decl(line) and token.find("()") != -1 and is_c(filename)):
        print color.RED + "You must specify \'void\' here "\
            + color.YELLOW + filename + ":l." + str(i)
        print color.RESET + token
        ccppfile.err += 1
    if (token.find("(void)") != -1 and is_c(filename) == False):
        print color.RED + "You must not specify \'void\' here "\
            + color.YELLOW + filename + ":l." + str(i)
        print color.RESET + token
        ccppfile.err += 1

def is_decl(line):
  return ((line.find("print") == -1 and
          line.find("int") != -1) or
          line.find("void") != -1 or
          line.find("char") != -1 or
          line.find("string") != -1 or
          line.find("std::string") != -1 or
          line.find("bool") != -1)


def includes(filename, line, i):
  tokens = line.split()
  for token in tokens:
    if (filename.find(".c") != -1):
      if (token == '#'):
        ccppfile.err += 1
        print color.RED + "Includes should follow \'#\' " + color.YELLOW +\
            filename + ":l." + str(i)
    else:
      if (token.find('#') != -1 and token != "#ifndef" and token != '#'):
        ccppfile.err += 1
        print color.RED + "\'#\' Should be followed by a whitespace "\
            + color.YELLOW + filename + ":l." + str(i)

def is_c_or_cpp(filename):
  if (filename.find(".cc") == -1 and\
      filename.find(".hh") == -1 and\
      filename.find(".c") == -1 and\
      filename.find(".h") == -1 and\
      filename.find(".cpp") == -1 and\
      filename.find(".hxx") == -1):
    return False
  else:
    return True

def is_c(filename):
  if (filename.find(".cc") != -1 or\
      filename.find(".hh") != -1 or\
      filename.find(".cpp") != -1 or\
      filename.find(".hxx") != -1):
    return False
  else:
    return True

def is_header(filename):
  if (filename.find(".h") != -1):
    return True
  return False

def count_lines(filename, lines, nblines):
  count = -1
  start = False
  fun = 0
  for i in range (1, nblines - 1):
    if (start == True):
      count += 1
      if (lines[i].find('{') != -1 or lines[i].find('}') != -1):
        count -= 1
    if (len(lines[i]) == 2 and lines[i] == "{\n"):
      fun = i + 2
      start = True
    if (len(lines[i]) == 2 and lines[i] == "}\n"):
      if (not(is_header(filename)) and is_c(filename)):
        if (count > 25):
          print color.RESET + str(count) + color.RED + " lines out of" +\
              color.GREEN + " 25 " + color.RED + "max in "\
              + color.YELLOW + filename + ":l." + str(fun) + "-" + str(i)
          ccppfile.err += 1
      if (not(is_header(filename)) and not(is_c(filename))):
        if (count > 50):
          print color.RESET + str(count) + color.RED + " lines out of" +\
              color.GREEN + " 50 " + color.RED + "max in "\
              + color.YELLOW + filename + ":l." + str(fun) + "-" + str(i)
          ccppfile.err += 1
      start = False
      count = -1

def Trailing_Whitespace(filename, lines, nblines):
  is_an_err_here = False
  for i in range (1, nblines - 2):
    if ((lines[i] == "\n") and (lines[i + 1] == "\n")):
      print color.RED + "Trailing whitespace " + color.YELLOW + filename + ": " \
        "l."+ str(i + 1) + "-" + str(i + 2)
      is_an_err_here = True
  if (is_an_err_here):
    ccppfile.err += 1

############################### MAIN FUNCTIONS ################################

# This one checks a single file's coding style
err = 0
def Check_Coding_Style(filename):
  global err
  [n, lines] = get_line(filename)
  count_lines(filename, lines, n)
  Trailing_Whitespace(filename, lines, n)
  for i in range (1, n - 1):
    if (lines[i - 1][0] == '#'):
      includes(filename, lines[i - 1], i)
    else:
      EvalLine(filename, lines[i - 1], i)
      Count80col(filename, lines[i - 1], i)
  if (ccppfile.err != 0):
    err += ccppfile.err
    ccppfile.listerr.append(filename)
    print color.RED
    print str(ccppfile.err) + color.BLUE + " Coding style errors found in "\
        + color.YELLOW + filename
    print color.BLUE + "Please check this file again to avoid troubles\n\n"
    if (i != n - 2):
      print "-------------------------------------------------------------\n"

def print_help():
  print "Usage: moulinette [FILE | FOLDER]"
  print "\"moulinette\" is the alias corresponding to the binary"\
      + " \"./main.py\""
  print "If no args given, the program will apply in the current directory"
  print


# This one will apply recursively the previous one on each functions in this
# folder
def main():
  if (len(sys.argv) >= 2):
    if (sys.argv[1] == "help"):
      print_help()
      exit (0)
    for i in range (1, len(sys.argv)):
      if (is_c_or_cpp(sys.argv[i])):
        Check_Coding_Style(sys.argv[i])
      else:
        if (sys.argv[i].find('.') == -1):
          print color.YELLOW + os.getcwd().replace("/home/kev", "~") \
              + '/' + sys.argv[i]
        else:
          print color.YELLOW + os.getcwd().replace("/home/kev", "~")
        tmp = ccppfile.found
        for filename in glob.glob(sys.argv[i] + '*'):
          filename.replace("/home/kev", "~")
          filetreat(filename)
        if (ccppfile.found == tmp):
          print color.RED + "No C or C++ file found in this folder\n" + color.RESET
      if (i != len(sys.argv) - 1):
        print color.RESET +\
              "\t==========================================================="
        print "\t===========================================================\n"
    else:
      if (ccppfile.found > 0 and err == 0):
        print color.GREEN + "No coding style error in this folder!\n" + color.RESET
        exit(0)
      elif (ccppfile.found > 0 and err != 0):
        print
        print color.BLUE + "!!!! SUMMARY !!!!\n"
        print color.RED + str(err) + color.BLUE +\
            " Total coding style errors found in :"
        for f in ccppfile.listerr:
          print color.YELLOW + '- ' + f + color.RESET
        exit (1)
      exit (0)
  else:
    for filename in glob.glob('*'):
      filename.replace("/home/kev", "~")
      filetreat(filename)
    if (ccppfile.found == 0):
      print color.RED + "No C or C++ file found\n" + color.RESET
    else:
      if (ccppfile.found > 0 and err == 0):
        print color.GREEN + "No coding style errors. Well done!\n" + color.RESET
        exit(0)
      else:
        print
        print color.BLUE + "!!!! SUMMARY !!!!\n"
        print color.RED + str(err) + color.BLUE +\
            " Total coding style errors found in :"
      for f in ccppfile.listerr:
        print color.YELLOW + '- ' + f + color.RESET
      exit(1)
    exit(0)

# Calls the main
if __name__ == '__main__':
  main()

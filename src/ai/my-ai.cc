#include "my-ai.hh"
#include "../chess-utils.hh"
#include <algorithm>

//#define DEBUG
MyAi::MyAi(Color color)
        : Ai(color)
{
    chessboard_.init();
    Position start = Position(Position::ANNA, Position::EINS);
    start_move_ = Move(start, start);
    color_ = color;
    last_opponent_move_ = Move(start, start);
    name_ = "bartol_j";
}

MyAi::~MyAi()
{
    chessboard_.clean();
}

/**
 * get all possible moves that the player with the color
 * color_ can do from a given chessboard_.
**/
std::vector<Move> check_castling(ChessBoard c, Color color)
{
    ChessBoard chessboard = ChessBoard(c.get_pieces());
    std::vector<Move> vect;
    Piece* king = get_piece(PieceType::KING, color, c.get_pieces());

    if (king)
    {
        Position::File f = king->getPosition().file_get();
        Position::Rank rp;
        rp = static_cast<Position::Rank>(king->getPosition().rank_get() + 2);
        Position::Rank rm;
        rm = static_cast<Position::Rank>(king->getPosition().rank_get() - 2);
        Move m = Move(king->getPosition(), Position(f, rp));
        if (chessboard.castling(m, color))
            vect.push_back(m);
        m = Move(king->getPosition(), Position(f, rm));
        if (chessboard.castling(m, color))
            vect.push_back(m);
    }
    return vect;
}

std::vector<Move> MyAi::get_all_move(Color color, ChessBoard cb)
{
    std::vector<Move> vect;
    for (auto piece : cb.get_color_pieces(color))
    {
        if (piece->getPieceType() == PieceType::KING)
        {
            piece->get_valid_Move(cb.get_pieces());
            for (auto pos : piece->get_possible_Move()) {
                vect.push_back(Move(piece->getPosition(), pos));
            }
        }
        else
            for (auto pos : piece->get_valid_Move(cb.get_pieces()))
                vect.push_back(Move(piece->getPosition(), pos));
    }
    for (auto i : check_castling(cb, color))
        vect.push_back(i);
    return vect;
}

int MyAi::count_total_moves(Color color, ChessBoard cb)
{
    return get_all_move(color, cb).size();
}

const std::string& MyAi::name_get() const
{
    return name_;
}

int MyAi::piece_benefit(ChessBoard cb, Color ally, PieceType type)
{
    int count = 0;
    for (auto piece : cb.get_pieces())
    {
        if (piece->getPieceType() == type)
        {
            if (piece->getColor() == ally)
                ++count;
            else
                --count;
        }
    }
    return count;
}

ChessBoard MyAi::simulate(ChessBoard cb, Move move)
{
    cb.make_move(move);
    return cb;
}

void MyAi::update()
{
    if (!(last_opponent_move_ == start_move_))
        chessboard_.make_move(last_opponent_move_);
}

float MyAi::eval(Color ally, ChessBoard cb)
{
  Color ennemy = (ally == Color::WHITE)? Color::BLACK : Color::WHITE;
  float count = 200 * piece_benefit(cb, ally, PieceType::KING);
  count += 10 * piece_benefit(cb, ally, PieceType::QUEEN);
  count += 5 * piece_benefit(cb, ally, PieceType::ROOK);
  count += 3 * piece_benefit(cb, ally, PieceType::BISHOP);
  count += 3 * piece_benefit(cb, ally, PieceType::KNIGHT);
  count += piece_benefit(cb, ally, PieceType::PAWN);
  count += 0.1 * (count_total_moves(ally, cb) - count_total_moves(ennemy, cb));
  return count;
}

std::vector<Move> MyAi::check_reaction(ChessBoard cb,
    King k, std::vector<Piece*> threats)
{
    Color ennemy = (k.getColor() == Color::WHITE)? Color::BLACK : Color::WHITE;
    std::vector<Move> vect;

    for (auto possible : k.get_valid_Move(cb.get_pieces()))
        if (!cb.pos_threatened(possible, ennemy))
            vect.push_back(Move(k.getPosition(), possible));

    for (auto threatener : threats)
    {
        for (auto move : get_all_move(k.getColor(), cb))
        {
            if (move.start_get() != k.getPosition())
            {
                if (move.end_get() == threatener->getPosition())
                    vect.push_back(move);
                else if (between(k.getPosition(), threatener->getPosition(),
                      move.end_get()))
                    vect.push_back(move);
            }
        }
    }
    return vect;
}

float MyAi::minimax(ChessBoard cb, Color ally, int depth)
{
    std::vector<Move> moves;
    Color ennemy = (ally == Color::WHITE)? Color::BLACK : Color::WHITE;
    if (checkstate(cb, ally) == 1)
    {
#ifdef DEBUG
        std::cout << "CHECKMATE" << std::endl;
#endif
        if (ally == Color::WHITE)
            return -40960;
        else
            return 40960;
    }
    else if (checkstate(cb, ally) == 2)
    {
        if (ally == Color::WHITE)
            return 40960;
        else
            return -40960;
    }
    else if (checkstate(cb, ally) == 0)
    {
        King k = cb.get_king(ally);
        auto threats = cb.piece_threatened(k.getPosition(), ennemy);
        moves = check_reaction(cb, k, threats);
    }
    else
        moves = get_all_move(ally, cb);

    if (depth == 0)
        return eval(ally, cb);

    float bestVal;
    if (ally == Color::WHITE)
        bestVal = -40960;
    else
        bestVal = 40960;


#ifdef DEBUG
    if (count_total_moves(ally, cb) == 0)
        std::cout << "IMPOSSIBLE" << std::endl;
#endif

    Move test;
    for (Move move : moves)
    {
        int old = bestVal;
        auto copy = ChessBoard(cb.get_pieces());
        copy = simulate(copy, move);
        float val = minimax(copy, ennemy, depth - 1);
        if (ally == Color::WHITE)
            bestVal = std::max(val, bestVal);
        else
            bestVal = std::min(val, bestVal);
        if (old != bestVal)
            test = move;
    }
    move_ = test;
    return bestVal;
}

Move MyAi::move_get()
{
    update();
    minimax(chessboard_, color_, 2);
    chessboard_.make_move(move_);
    return move_;
}

AI_EXPORT(MyAi)

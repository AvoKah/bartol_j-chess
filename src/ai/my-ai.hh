#ifndef MY_AI_HH_
# define MY_AI_HH_

# include "../chessboard/chessboard.hh"
# include "../include/ai.hh"

class MyAi: public Ai
{
public:
    MyAi(Color color);
    ~MyAi();

public:
    const std::string& name_get() const override;
    Move move_get() override;

public:
    std::vector<Move> get_all_move(Color color, ChessBoard cb);
    int piece_benefit(ChessBoard cb, Color color, PieceType type);
    int count_total_moves(Color color, ChessBoard cb);
    ChessBoard simulate(ChessBoard cb, Move move);
    void update();
    float eval(Color ally, ChessBoard cb);
    float minimax(ChessBoard cb, Color ally, int depth);
    std::vector<Move> check_reaction(ChessBoard cb, King k,
        std::vector<Piece*> threats);

protected:
    Move move_;
    Move start_move_;
    Color color_;
    ChessBoard chessboard_;
    std::string name_;
};

#endif /* !MY_AI_HH_ */

#ifndef CHESS_AI_HH_
# define CHESS_AI_HH_

# include "../chessboard/chessboard.hh"
# include "../include/ai.hh"

class ChessAi: public Ai
{
public:
  ChessAi(Color color);
  ~ChessAi();

  /// Algo minimax
  int minimax(int count = 0, int profondeur = 20);
  int score(int count);
  Move move_get() override;
  Move random();
  void update();

  const std::string& name_get() const override;

protected:
  std::string name_;
  Color color_;
  Move start_move_;
  std::tuple<Position, Position> move_;
  ChessBoard chessboard_;
};

#endif /* !CHESS_AI_HH_ */

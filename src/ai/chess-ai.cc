#include "chess-ai.hh"
#include <cstdlib>
#include <ctime>

ChessAi::ChessAi(Color color)
  : Ai(color)
  , color_(color)
{
    chessboard_.init();
    Position start = Position(Position::File::ANNA, Position::Rank::EINS);
    start_move_ = Move(start, start);
    last_opponent_move_ = Move(start, start);
    name_ = "bartol_j";
}

ChessAi::~ChessAi()
{
    chessboard_.clean();
}

int ChessAi::minimax(int count, int profondeur)
{
  if (/*cb.game_end() || */profondeur == 0)
    return score(count);
  count += 1;
  std::vector<int> scores;
  std::vector<std::tuple<Position, Position>> moves;
  for (auto m: chessboard_.get_all_move())
  {
    chessboard_.make_move(Move(std::get<0>(m), std::get<1>(m)));
    scores.push_back(minimax(count, profondeur - 1));
    moves.push_back(m);
  }

  if (this->color_ == Color::WHITE)
  {
    int max = 0;
    int index = 0;
    for (int s: scores)
    {
      if (s > scores[max])
        max = index;
      index++;
    }
    this->move_ = moves[max];
    return scores[max];
  }
  else
  {
    int min = 0;
    int index = 0;
    for (int s: scores)
    {
      if (s < scores[min])
        min = index;
      index++;
    }
    this->move_ = moves[min];
    return scores[min];
  }
}

int ChessAi::score(int count)
{
  /*if (who_win("white"))
    return 10 - count;
  else if (who_win("black")) */
    return count - 10;
 /* else
    return 0;*/
}

const std::string& ChessAi::name_get() const
{
    return name_;
}

Move ChessAi::move_get()
{
    update();
    Move rand = random();
    chessboard_.make_move(rand);
    return rand;
}

void ChessAi::update()
{
    if (!(last_opponent_move_ == start_move_))
        chessboard_.make_move(last_opponent_move_);
}

Move ChessAi::random()
{
    auto pieces = chessboard_.get_color_pieces(color_);
    int pi = std::rand() % (int) pieces.size();

    while (pieces[pi]->get_valid_Move(chessboard_.get_pieces()).size() == 0)
        pi = std::rand() % (int) pieces.size();

    auto moves = pieces[pi]->get_valid_Move(chessboard_.get_pieces());
    int mi = std::rand() % (int) moves.size();

    Move out = Move(pieces[pi]->getPosition(), moves[mi]);

    std::cout << chessboard_;

    return out;
}

//AI_EXPORT(ChessAi)

#include "chessgame-manager.hh"
#include "listener-manager.hh"
#include "adapter/chessboard-adapter.hh"
#include "referee/referee.hh"

//#define DEBUG
//#define ROUND

ChessGameManager::ChessGameManager(Player* p1, Player* p2)
  : player1_(p1)
  , player2_(p2)
{
  chessboard_.init();
  ChessboardAdapter adapter = ChessboardAdapter(chessboard_);
  ListenerManager::get_instance().register_chessboard_interface(adapter);
}

ChessGameManager::~ChessGameManager()
{}

void ChessGameManager::play()
{
#ifdef DEBUG
    std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> START" << std::endl;
    std::cout << chessboard_;
#endif
#ifdef ROUND
    int tmp = 0;
#endif
    Referee referee = Referee(chessboard_);
  while (true)
  {
#ifdef ROUND
    std::cout << "\n[ROUND : " << (tmp + 1) << "]" << std::endl;
#endif
    Move white_move = player1_->move_get();
    referee.before_move(white_move, Color::WHITE);

    // PROMOTION FIXME

    chessboard_.make_move(white_move);
    player2_->last_opponent_move_set(white_move);
    referee.update(chessboard_);

    if (referee.checkmate(Color::BLACK))
    {
        ListenerManager::get_instance().on_player_mat(Color::BLACK);
        break;
    }
    else if (referee.status() == "CHECK")
        referee.check(Color::BLACK);

#ifdef DEBUG
    if (tmp >= 0)
    {
        std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHITE" << std::endl;
        std::cout << white_move << std::endl;
        std::cout << chessboard_;
    }
#endif

    Move black_move = player2_->move_get();
    referee.before_move(black_move, Color::BLACK);

    chessboard_.make_move(black_move);
    player1_->last_opponent_move_set(black_move);
    referee.update(chessboard_);

    if (referee.checkmate(Color::WHITE))
    {
        ListenerManager::get_instance().on_player_mat(Color::WHITE);
        break;
    }
    else if (referee.status() == "CHECK")
        referee.check(Color::WHITE);

#ifdef DEBUG
    if (tmp >= 0)
    {
        std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BLACK" << std::endl;
        std::cout << black_move << std::endl;
        std::cout << chessboard_;
    }
#endif
#ifdef ROUND
    tmp++;
#endif
  }

  chessboard_.clean();
}

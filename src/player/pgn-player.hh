#ifndef PGN_PLAYER_HH
# define PGN_PLAYER_HH

# include "../include/player.hh"
# include "../include/move.hh"
# include "../pgn/pgn-parser.hh"
# include "../pieces/piece.hh"

# include <vector>
# include <string>

class PgnPlayer: public Player
{
    public:
        PgnPlayer(Color color, std::string filename);
        ~PgnPlayer();

        Move move_get() override;
        Move create_move(int index);

        Move castling(bool big);
        PieceType type(std::string input, int index);
        Position end_pos(std::string input, int index);

        Position start_position(Piece *piece, std::string dist = "0");
        Position start_piece_position(Piece *piece, std::string dist);
        Position start_pawn_position(Piece *piece, std::string dist);
        Piece* create_piece(PieceType piece_type, Position p);

        void dump();
    private:
        int next_move_;
        PgnParser pgn_parser_;
        std::string name_;
        std::vector<std::string> moves_;
        std::vector<Piece*> chessboard_;
};

#endif /* !PGN_PLAYER_HH */

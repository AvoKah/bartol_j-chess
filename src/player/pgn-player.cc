#include "pgn-player.hh"
#include "../pieces/bishop.hh"
#include "../pieces/king.hh"
#include "../pieces/knight.hh"
#include "../pieces/pawn.hh"
#include "../pieces/queen.hh"
#include "../pieces/rook.hh"
#include "../include/position.hh"
#include "../chess-utils.hh"

PgnPlayer::PgnPlayer(Color color, std::string filename)
    : Player(color)
    , next_move_(0)
    , pgn_parser_(filename)
{
    pgn_parser_.parse();
    name_ = pgn_parser_.name_get(color);
    moves_ = pgn_parser_.piece_moves_get(color);
    init(chessboard_);
}

PgnPlayer::~PgnPlayer()
{
    for (auto* i : chessboard_)
        delete i;
}

Move PgnPlayer::move_get()
{
    if (color_ != WHITE || next_move_ >= 1)
        make_move(last_opponent_move_, chessboard_);
    Move m = create_move(next_move_);
    next_move_++;
    make_move(m, chessboard_);
    return m;
}

Move PgnPlayer::create_move(int index)
{
    std::string move = moves_[index];

    if (move == "O-O-O")
        return castling(true);
    else if (move == "O-O")
        return castling(false);

    int i = 0;
    PieceType t = type(move, i);
    if (t != PieceType::PAWN)
        i++;

    int distinct = i;
    i = is_distinguished(move, i);
    distinct = i - distinct;
    std::string dist;
    if (distinct == 0)
        dist = "0";
    else
        dist = move.substr(i - distinct, distinct);

    if (move[i] == 'x')
        i++;

    Position p = end_pos(move, i);
    i += 2;

    PieceType promotion = PieceType::NONE;;
    if (is_promoted(move, i))
        promotion = type(move, i + 1);

    Piece* piece = create_piece(t, p);
    Move out = Move(start_position(piece, dist), p, promotion);
    delete piece;

    return out;
}

Move PgnPlayer::castling(bool big)
{
    Position::File king = Position::File::EVA;
    Position::File big_castle = Position::File::CESAR;
    Position::File lil_castle = Position::File::GUSTAV;

    Position::Rank white = Position::Rank::EINS;
    Position::Rank black = Position::Rank::ACHT;
    if (color_ == Color::WHITE)
    {
        if (big)
            return Move(Position(king, white), Position(big_castle, white));
        else
            return Move(Position(king, white), Position(lil_castle, white));
    }
    else
    {
        if (big)
            return Move(Position(king, black), Position(big_castle, black));
        else
            return Move(Position(king, black), Position(lil_castle, black));
    }
}

Position PgnPlayer::start_position(Piece* piece, std::string dist)
{
    if (piece->getPieceType() != PieceType::PAWN)
        return start_piece_position(piece, dist);
    else
        return start_pawn_position(piece, dist);
}

Position PgnPlayer::start_piece_position(Piece *piece, std::string dist)
{
    auto positions = piece->get_Move();
    for (auto pos : positions)
    {
        Piece* p2 = get_piece(pos, chessboard_);
        if (p2)
        {
            if ((piece->getPieceType() == p2->getPieceType())
                    && piece->getColor() == p2->getColor())
            {
                auto check_pos = p2->get_valid_Move(chessboard_);
                for (auto cp : check_pos)
                {
                    if (dist == "0")
                    {
                        if (cp == piece->getPosition())
                            return pos;
                    }
                    else if ((dist != "0") && (dist.length() == 2))
                    {
                        auto f = static_cast<Position::File>(dist[0]);
                        auto r = static_cast<Position::Rank>(dist[1]);
                        Position start_pos = Position(f, r);
                        if (cp == piece->getPosition()
                            && p2->getPosition() == start_pos)
                            return pos;
                    }
                    else if ((dist != "0") && (dist.length() == 1))
                    {
                        if (is_file(dist[0]))
                        {
                            int tmp =
                                get_file(dist[0]);
                            auto f = static_cast<Position::File>(tmp);
                            if (cp == piece->getPosition()
                                && p2->getPosition().file_get() == f)
                                return pos;
                        }
                        else if (is_rank(dist[0]))
                        {
                            int tmp = std::stoi(dist);
                            auto r = static_cast<Position::Rank>(tmp);
                            if (cp == piece->getPosition()
                                && p2->getPosition().rank_get() == r)
                                return pos;
                        }
                    }
                }
            }
        }
    }
    return piece->getPosition();
}

Position PgnPlayer::start_pawn_position(Piece *piece, std::string dist)
{
    for (auto p : chessboard_)
    {
        if ((p->getPieceType() == PieceType::PAWN)
                && p->getColor() == color_)
        {
            for (auto pawn_pos : p->get_valid_Move(chessboard_))
            {
                if (dist == "0")
                {
                    if (piece->getPosition() == pawn_pos)
                        return p->getPosition();
                }
                else if ((dist != "0") && (dist.length() == 2))
                {
                    auto f = static_cast<Position::File>(dist[0]);
                    auto r = static_cast<Position::Rank>(dist[1]);
                    Position start_pos = Position(f, r);
                    if (piece->getPosition() == pawn_pos
                        && pawn_pos == start_pos)
                        return p->getPosition();
                }
                else if ((dist != "0") && (dist.length() == 1))
                {
                    if (is_file(dist[0]))
                    {
                        int tmp = get_file(dist[0]);
                        auto f = static_cast<Position::File>(tmp);
                        if (piece->getPosition() == pawn_pos
                            && p->getPosition().file_get() == f)
                            return p->getPosition();
                    }
                    else if (is_rank(dist[0]))
                    {
                        auto r = static_cast<Position::Rank>(dist[0]);
                        if (piece->getPosition() == pawn_pos
                            && p->getPosition().rank_get() == r)
                            return p->getPosition();
                    }
                }
            }
        }
    }
    return piece->getPosition();
}

Piece* PgnPlayer::create_piece(PieceType piece_type, Position p)
{
    switch (piece_type)
    {
        case 0:
            return new King(color_, p);
        case 1:
            return new Queen(color_, p);
        case 2:
            return new Rook(color_, p);
        case 3:
            return new Bishop(color_, p);
        case 4:
            return new Knight(color_, p);
        case 5:
            return new Pawn(color_, p);
        default:
            return nullptr;
    }
}

PieceType PgnPlayer::type(std::string input, int index)
{
    PieceType pt;
    switch (input[index])
    {
        case 'K':
            pt = PieceType::KING;
            break;
        case 'Q':
            pt = PieceType::QUEEN;
            break;
        case 'B':
            pt = PieceType::BISHOP;
            break;
        case 'N':
            pt = PieceType::KNIGHT;
            break;
        case 'R':
            pt = PieceType::ROOK;
            break;
        default :
            pt = PieceType::PAWN;
            break;
    }

    return pt;
}

Position PgnPlayer::end_pos(std::string input, int index)
{
    Position::File f;
    Position::Rank r;

    switch (input[index])
    {
        case 'a':
            f = Position::File::ANNA;
            break;
        case 'b':
            f = Position::File::BELLA;
            break;
        case 'c':
            f = Position::File::CESAR;
            break;
        case 'd':
            f = Position::File::DAVID;
            break;
        case 'e':
            f = Position::File::EVA;
            break;
        case 'f':
            f = Position::File::FELIX;
            break;
        case 'g':
            f = Position::File::GUSTAV;
            break;
        case 'h':
            f = Position::File::HECTOR;
            break;
    }

    switch (input[index + 1])
    {
        case '1':
            r = Position::Rank::EINS;
            break;
        case '2':
            r = Position::Rank::ZWEI;
            break;
        case '3':
            r = Position::Rank::DREI;
            break;
        case '4':
            r = Position::Rank::VIER;
            break;
        case '5':
            r = Position::Rank::FUNF;
            break;
        case '6':
            r = Position::Rank::SECHS;
            break;
        case '7':
            r = Position::Rank::SIEBEN;
            break;
        case '8':
            r = Position::Rank::ACHT;
            break;
    }

    return Position(f, r);
}

void PgnPlayer::dump()
{
    std::cerr << "PLAYER :" << name_ << std::endl;
    for (auto i : moves_)
        std::cerr << i << " | ";
    std::cerr << std::endl;
}

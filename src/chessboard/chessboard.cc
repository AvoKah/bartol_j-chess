#include "chessboard.hh"
#include "../pieces/bishop.hh"
#include "../pieces/king.hh"
#include "../pieces/knight.hh"
#include "../pieces/pawn.hh"
#include "../pieces/queen.hh"
#include "../pieces/rook.hh"
#include "../chess-utils.hh"
#include <algorithm>
#include <cstdlib>

ChessBoard::ChessBoard()
{}

ChessBoard::ChessBoard(const ChessBoard& chessboard)
{
    const auto p = chessboard.get_pieces();
    pieces_ = p;
}

ChessBoard::ChessBoard(std::vector<Piece*> pieces)
{
    for (auto p : pieces)
        pieces_.push_back(create_piece(p->getPieceType(),
                                    p->getColor(),
                                    p->getPosition()));
}

ChessBoard::~ChessBoard()
{}

/**
 * \brief This functiton will set all pieces of a chessboard at his place
 */
void ChessBoard::init()
{
  pieces_.push_back(new King(Color::WHITE, white(Position::EVA)));
  pieces_.push_back(new King(Color::BLACK, black(Position::EVA)));

  pieces_.push_back(new Queen(Color::WHITE, white(Position::DAVID)));
  pieces_.push_back(new Queen(Color::BLACK, black(Position::DAVID)));

  pieces_.push_back(new Bishop(Color::WHITE, white(Position::CESAR)));
  pieces_.push_back(new Bishop(Color::WHITE, white(Position::FELIX)));
  pieces_.push_back(new Bishop(Color::BLACK, black(Position::CESAR)));
  pieces_.push_back(new Bishop(Color::BLACK, black(Position::FELIX)));

  pieces_.push_back(new Knight(Color::WHITE, white(Position::BELLA)));
  pieces_.push_back(new Knight(Color::WHITE, white(Position::GUSTAV)));
  pieces_.push_back(new Knight(Color::BLACK, black(Position::BELLA)));
  pieces_.push_back(new Knight(Color::BLACK, black(Position::GUSTAV)));

  pieces_.push_back(new Rook(Color::WHITE, white(Position::ANNA)));
  pieces_.push_back(new Rook(Color::WHITE, white(Position::HECTOR)));
  pieces_.push_back(new Rook(Color::BLACK, black(Position::ANNA)));
  pieces_.push_back(new Rook(Color::BLACK, black(Position::HECTOR)));

  for (Position::File it = Position::ANNA; it < Position::FILE_LAST; ++it)
  {
    pieces_.push_back(new Pawn(Color::WHITE, white(it, true)));
    pieces_.push_back(new Pawn(Color::BLACK, black(it, true)));
  }
}

PieceType ChessBoard::get_type(Position cell)
{
  for (auto i : pieces_)
    if (i->getPosition() == cell)
      return i->getPieceType();
  return PieceType::NONE;
}

Piece* ChessBoard::get_piece(Position cell)
{
  for (auto i : pieces_)
    if (i->getPosition() == cell)
      return i;
  return nullptr;
}

std::vector<Piece*> ChessBoard::get_pieces()
{
  return pieces_;
}

std::vector<Piece*> ChessBoard::get_pieces() const
{
  return pieces_;
}

void ChessBoard::pieces_set(std::vector<Piece*> pieces)
{
    pieces_ = pieces;
}

bool ChessBoard::castling(Move move, Color kingColor)
{
  King* king = dynamic_cast<King*>(get_piece(move.start_get()));
  if (king->has_moved())
    return false;
  auto from = move.start_get();
  auto rank = from.rank_get();
  if (from.file_get() != Position::EVA)
    return false;
  int sign = move.end_get().file_get() - from.file_get();
  Position::File f = from.file_get();
  if (sign > 0)
  {
    ++f;
    for (;
        (get_type(Position(f, rank)) != PieceType::ROOK
         || f != Position::HECTOR) && f < Position::FILE_LAST; ++f)
      if (get_type(Position(f, rank)) != PieceType::NONE)
        return false;
  }
  else
  {
    --f;
    for (;
        (get_type(Position(f, rank)) != PieceType::ROOK
         || f != Position::ANNA) && f > Position::FILE_FIRST; --f)
      if (get_type(Position(f, rank)) != PieceType::NONE)
        return false;
  }
  if (get_type(Position(f, rank)) != PieceType::ROOK)
    return false;

  if ((from.rank_get() == Position::EINS && kingColor == Color::WHITE)
      || (from.rank_get() == Position::ACHT && kingColor == Color::BLACK))
  {
    make_move(move, true);
    auto tmp = move.end_get().file_get();
    if (sign > 0)
      make_move(Move(Position(f, rank), Position(--tmp, rank)), true);
    else
      make_move(Move(Position(f, rank), Position(++tmp, rank)), true);
    return true;
  }
  else
    return false;
}

void ChessBoard::make_promotion(Piece* p, PieceType promotion)
{
  auto tmp = create_piece(promotion, p->getColor(),
      p->getPosition());
  update_piece(p, tmp);
}

void ChessBoard::checkstate(Color color)
{
  Color ennemies = (color == Color::WHITE)? Color::BLACK : Color::WHITE;
  Piece* kpiece = get_piece(get_king(color).getPosition());
  std::vector<Position> vect = kpiece->get_possible_Move();
  for (auto i : kpiece->get_possible_Move())
    if (pos_threatened(i, ennemies))
      vect.erase(std::find(vect.begin(), vect.end(), i));
  // Remove this if you got troubles.
  kpiece->set_possible_Move(vect);
}

bool ChessBoard::check_promotion(Piece* piece)
{
    if (piece->getColor() == Color::BLACK
        && piece->getPosition().rank_get() == Position::EINS)
        return true;
    if (piece->getColor() == Color::WHITE
        && piece->getPosition().rank_get() == Position::ACHT)
        return true;
    return false;
}

bool ChessBoard::make_move(Move move, bool setvalid)
{
  Piece* piece = get_piece(move.start_get());
  piece->get_valid_Move(pieces_);

 if (piece->getPieceType() == PieceType::KING)
    checkstate(piece->getColor());

  std::vector<Position> vect = piece->get_possible_Move();
  bool valid_move = setvalid;

  for (Position pos : vect)
    if (pos == move.end_get())
      valid_move = true;
  if (valid_move)
  {
    if (piece->getPieceType() == PieceType::KING)
      dynamic_cast<King*>(piece)->moved();

    Position p = move.end_get();
    Piece* tmp_piece = get_piece(p);

    if (tmp_piece)
    {
      pieces_.erase(std::find(pieces_.begin(), pieces_.end(), get_piece(p)));
      delete tmp_piece;
    }

    piece->setPosition(p);

      if (move.promotion_get() != PieceType::NONE)
          make_promotion(piece, move.promotion_get());
      else if (check_promotion(piece))
          make_promotion(piece, PieceType::QUEEN);

    last_move_ = move;
    return true;
  }
  else if (piece->getPieceType() == PieceType::KING
     && std::abs(move.start_get().file_get() - move.end_get().file_get()) == 2)
    return castling(move, piece->getColor());
  return false;
}

bool ChessBoard::pos_threatened(Position pos, Color piece_moved)
{
    Color friend_ = (piece_moved == Color::WHITE)? Color::BLACK : Color::WHITE;
    Piece* kpiece = get_piece(get_king(friend_).getPosition());
    Position old = kpiece->getPosition();

    Piece* aux = get_piece(pos);
    bool erased = false;
    if (aux && (old != pos))
    {
        pieces_.erase(std::find(pieces_.begin(), pieces_.end(), aux));
        erased = true;
    }
    kpiece->setPosition(pos);

    auto pieces = get_color_pieces(piece_moved);
    for (auto piece : pieces)
        for (auto move : piece->get_valid_Move(get_pieces()))
            if (move == pos)
            {
                if (erased)
                    pieces_.push_back(aux);
                kpiece->setPosition(old);
                return true;
            }

    if (erased)
        pieces_.push_back(aux);
    kpiece->setPosition(old);
    return false;
}

std::vector<Piece*> ChessBoard::piece_threatened(Position pos,
                                                    Color piece_moved)
{
  std::vector<Piece*> threats;
  auto pieces = get_color_pieces(piece_moved);
  for (auto piece : pieces)
    for (auto move : piece->get_valid_Move(get_pieces()))
      if (move == pos)
          threats.push_back(piece);

  return threats;
}

void ChessBoard::update_piece(Piece* oldPiece, Piece* newPiece)
{
  for (auto it = pieces_.begin(); it < pieces_.end(); ++it)
  {
    if ((*it) == oldPiece)
    {
      pieces_.erase(it);
      pieces_.push_back(newPiece);
      return;
    }
  }
}

std::vector<Piece*> ChessBoard::get_color_pieces(Color color)
{
  std::vector<Piece*> vect;
  for (auto i : pieces_)
    if ((*i).getColor() == color)
      vect.push_back(i);
  return vect;
}

King ChessBoard::get_king(Color color)
{
  King* k = nullptr;
  for (auto i : get_color_pieces(color))
  {
    if ((*i).getPieceType() == PieceType::KING)
    {
      k = new King(color, (*i).getPosition());
      return *k;
    }
  }
  return *k;
}

void ChessBoard::clean()
{
    for (auto i : pieces_)
        delete i;
}

std::vector<std::tuple<Position, Position>> ChessBoard::get_all_move()
{
  std::vector<std::tuple<Position, Position>> res;
  for (auto lp: this->pieces_)
    for (auto m: lp->get_valid_Move(this->pieces_))
      res.push_back(std::make_tuple(lp->getPosition(), m));
  return res;
}

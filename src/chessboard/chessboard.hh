#ifndef CHESSBOARD_HH_
# define CHESSBOARD_HH_

# include "../pieces/piece.hh"
# include "../pieces/king.hh"
# include "../include/move.hh"
# include <tuple>

class ChessBoard
{
  public:
    ChessBoard();
    ChessBoard(const ChessBoard& chessboard);
    ChessBoard(std::vector<Piece*> pieces);
    ~ChessBoard();

  public:
    void init();
    bool make_move(Move move, bool setvalid = false);
    void update_piece(Piece* oldpiece, Piece* newPiece);
    void make_promotion(Piece* p, PieceType promotion);
    void clean();

  public:
    bool castling(Move move, Color kingcolor);
    bool pos_threatened(Position pos, Color piece_moved);
    std::vector<Piece *> piece_threatened(Position cb, Color pos);
    bool en_passant(Piece* piece, Position dest);
    void checkstate(Color color);
    bool check_promotion(Piece* piece);

  public:
    PieceType get_type(Position cell);
    Piece* get_piece(Position cell);
    std::vector<Piece*> get_pieces();
    std::vector<Piece*> get_pieces() const;
    std::vector<Piece*> get_color_pieces(Color color);
    King get_king(Color color);
    void pieces_set(std::vector<Piece*> pieces);

  public:
    std::vector<std::tuple<Position, Position>> get_all_move();

  protected:
    std::vector<Piece*> pieces_;
    Move last_move_;
};

inline std::ostream& operator<<(std::ostream& ostr, ChessBoard board);

# include "chessboard.hxx"

#endif /* !CHESSBOARD_HH_ */

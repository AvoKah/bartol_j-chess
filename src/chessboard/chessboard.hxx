inline std::ostream& operator<<(std::ostream& ostr, ChessBoard board)
{
  Position pos;
  for (int i = 1; i < 9; i++)
  {
    for (int j = 1; j < 9; j++)
    {
      ostr << "|";
      auto f = static_cast<Position::File>(j);
      auto r = static_cast<Position::Rank>(9 - i);
      pos = Position(f, r);
      Piece* p = board.get_piece(pos);
      if (p)
        ostr << *p;
      else
        ostr << " ";
    }
    ostr << "| " << (9 - i) << std::endl;
  }
 ostr << std::endl << " A B C D E F G H\n" << "--------------------\n"
   << std::endl;
 return ostr;
}

#ifndef CHESS_UTILS_HH
# define CHESS_UTILS_HH

# include <string>
# include "include/position.hh"
# include "include/move.hh"
# include "pieces/piece.hh"
# include "chessboard/chessboard.hh"

/**
 * Contains all the useful functions that we use a lot of time
 * in different classes
 */
void error_(std::string message, int exit_code);

bool is_file(char c);
int get_file(char c);
bool is_rank(char c);
bool is_piece(char c);
bool is_promoted(std::string input, int index);
int is_distinguished(std::string input, int index);

Piece* get_piece(Position cell, std::vector<Piece*> chessboard);
Piece* get_piece(PieceType type, Color color, std::vector<Piece*> chessboard);
PieceType get_type(Position cell, std::vector<Piece*> pieces);
Position white(Position::File p, bool is_pawn = false);
Position black(Position::File p, bool is_pawn = false);
void init(std::vector<Piece*>& pieces);
void make_move(Move move, std::vector<Piece*>& pieces);
Piece* create_piece(PieceType type, Color c, Position p);
bool checkmate(ChessBoard chessboard_, Color color);
bool between(Position start, Position end, Position bet);
int checkstate(ChessBoard chessboard_, Color color);

#endif /* !CHESS_UTILS_HH */

#include "referee.hh"
#include "../listener-manager.hh"
#include "../pieces/piece.hh"
#include <vector>
#include <algorithm>
#include <string>

Referee::Referee(ChessBoard chessboard)
  : chessboard_(chessboard)
{}

Referee::~Referee()
{}

void Referee::update(ChessBoard chessboard)
{
  chessboard_ = chessboard;
}

void Referee::before_move(Move move, Color c)
{
    if (is_taken(move))
        taken(move);
    else if (castling(move))
        on_castling(move, c);
    else
        on_move(move);
}

bool Referee::check_king()
{
  return check_king(Color::WHITE) || check_king(Color::BLACK);
}

bool Referee::check_king(Color c)
{
  Color d = (c == Color::WHITE)? Color::BLACK : Color::WHITE;
  return chessboard_.pos_threatened(chessboard_.get_king(c).getPosition(), d);
}

void Referee::check(Color c)
{
    ListenerManager::get_instance().on_player_check(c);
}

bool Referee::is_taken(Move move)
{
    Piece* piece = chessboard_.get_piece(move.end_get());
    return piece;
}

void Referee::taken(Move move)
{
    PieceType type = chessboard_.get_type(move.end_get());
    ListenerManager::get_instance().on_piece_taken(type, move.end_get());
    on_move(move);
}

bool Referee::castling(Move move)
{
  if (chessboard_.get_type(move.start_get()) == PieceType::KING
    && std::abs(move.start_get().file_get() - move.end_get().file_get()) == 2)
    return true;
  return false;
}

void Referee::on_castling(Move move, Color c)
{
    Move rook_move;
    Position::File end_file = move.end_get().file_get();
    Position start;
    Position end;
    if (move.end_get().file_get() == Position::File::CESAR)
    {
        ListenerManager::get_instance().on_queenside_castling(c);
        start = Position(Position::File::ANNA, move.start_get().rank_get());
        end = Position(++end_file, move.end_get().rank_get());
    }
    else
    {
        ListenerManager::get_instance().on_kingside_castling(c);
        start = Position(Position::File::HECTOR, move.start_get().rank_get());
        end = Position(--end_file, move.end_get().rank_get());
    }

    rook_move = Move(start, end);
    on_move(move);
    on_move(rook_move);
}

void Referee::on_move(Move move)
{
    PieceType pt = chessboard_.get_type(move.start_get());
    ListenerManager::get_instance().on_piece_moved(pt,
            move.start_get(),
            move.end_get());
}

bool Referee::checkmate(Color color)
{
  Color ennemies = (color == Color::WHITE)? Color::BLACK : Color::WHITE;
  Piece* kpiece = chessboard_.get_piece(
          chessboard_.get_king(color).getPosition());

  std::vector<Position> vect = kpiece->get_possible_Move();
  if (vect.size() == 0)
      return false;

  if (!chessboard_.pos_threatened(kpiece->getPosition(), ennemies))
      return false;

  std::vector<Position> threat;
  for (auto i : vect)
  {
    if (!chessboard_.pos_threatened(i, ennemies))
        return false;
    else
        threat.push_back(i);
  }

  auto mypieces = chessboard_.get_color_pieces(color);
  auto threats = chessboard_.piece_threatened(kpiece->getPosition(), ennemies);

  for (auto t : threats)
      for (auto p : mypieces)
          if (p->getPieceType() != PieceType::KING)
          {
              for (auto pos : p->get_valid_Move(chessboard_.get_pieces()))
              {
                  if (between(kpiece->getPosition(), t->getPosition(), pos))
                  {
                      return false;
                  }
              }
          }

  return true;
}

bool Referee::between(Position start, Position end, Position bet)
{
    int br = bet.rank_get();
    int sr = start.rank_get();
    int er = end.rank_get();

    int bf = bet.file_get();
    int sf = start.file_get();
    int ef = end.file_get();

    if ((sr == br) && (sr == er))
        return ((sf <= bf) && (bf <= ef)) || ((sf >= bf) && (bf >= ef));
    else if ((sf == bf) && (sf == ef))
        return ((sr <= br) && (br <= er)) || ((sr >= br) && (br >= er));
    else if (((sr < br) && (br < er)) || ((sr > br) && (br > er)))
        return ((sf < bf) && (bf < ef)) || ((sf > bf) && (bf > ef));

    return false;
}

std::string Referee::status()
{
  bool no_move = true;
  Color c = Color::WHITE;
  for (auto piece : chessboard_.get_color_pieces(c))
  {
    if (piece->get_possible_Move().size() != 0)
    {
      no_move = false;
      break;
    }
  }
  if (!no_move)
  {
    no_move = true;
    c = Color::BLACK;
    for (auto piece : chessboard_.get_color_pieces(c))
    {
      if (piece->get_possible_Move().size() != 0)
      {
        no_move = false;
        break;
      }
    }
  }
  if (no_move && !check_king(c))
    return "STALEMATE";
  else if (no_move && check_king(c))
    return "CHECKMATE";
  else if (!no_move && check_king())
    return "CHECK";
  return clear_;
}

#ifndef REFEREE_HH_
# define REFEREE_HH_

# include "../chessboard/chessboard.hh"
# include "../include/move.hh"

class Referee
{
  public:
    Referee(ChessBoard chessboard);
    ~Referee();
  public:
    void update(ChessBoard chessboard);
    void before_move(Move move, Color c);
    bool between(Position start, Position end, Position bet);
    bool checkmate(Color color);
    bool check_king();
    bool check_king(Color c);
    void check(Color c);
    std::string status();
    bool is_taken(Move move);
    void taken(Move move);
    void on_move(Move move);
    bool castling(Move move);
    void on_castling(Move move, Color c);
  protected:
    ChessBoard chessboard_;
    std::string clear_ = "CLEAR";
};

#endif /* !REFEREE_HH_ */

#include "listener-manager.hh"
#include <dlfcn.h>

ListenerManager::ListenerManager()
{}

ListenerManager::~ListenerManager()
{}

ListenerManager ListenerManager::listener_manager_ = ListenerManager();

void ListenerManager::error_(std::string message)
{
    std::cerr << message << dlerror() << std::endl;
    exit(1);
}

ListenerManager& ListenerManager::get_instance()
{
    return ListenerManager::listener_manager_;
}

void ListenerManager::load_listener(std::vector<std::string> listen_libs)
{
    for (auto l : listen_libs)
    {
        void* listener = dlopen(l.c_str(), RTLD_LAZY);
        std::cout << l.c_str() << "$" << std::endl;
        if (!listener)
            error_("Error : loading library : ");

        ListenerExport* lexport = (ListenerExport *) dlsym(listener,
                                                            "listener_plugin");
        const char* dlsym_err = dlerror();
        if (dlsym_err)
            error_("Error export");

        listeners_.push_back(lexport->create());
        opened_.push_back(listener);
    }
}

void ListenerManager::manager_clean()
{
    for (Listener* l : listeners_)
        delete(l);

    for (auto l : opened_)
        dlclose(l);
}

void
ListenerManager::register_chessboard_interface(const ChessboardInterface&
                                               chessboard_interface)
{
  for (auto l : listeners_)
    l->register_chessboard_interface(chessboard_interface);
}

void
ListenerManager::on_game_started()
{
  for (auto l : listeners_)
    l->on_game_started();
}

void
ListenerManager::on_game_finished()
{
  for (auto l : listeners_)
    l->on_game_finished();
}

void
ListenerManager::on_piece_moved(const PieceType& piece, const Position& from,
                               const Position& to)
{
  for (auto l : listeners_)
    l->on_piece_moved(piece, from, to);
}

void
ListenerManager::on_piece_taken(const PieceType& piece, const Position& at)
{
  for (auto l : listeners_)
    l->on_piece_taken(piece, at);
}

void
ListenerManager::on_piece_promoted(const PieceType& piece, const Position& p)
{
  for (auto l : listeners_)
    l->on_piece_promoted(piece, p);
}

void
ListenerManager::on_kingside_castling(const Color& color)
{
  for (auto l : listeners_)
    l->on_kingside_castling(color);
}

void
ListenerManager::on_queenside_castling(const Color& color)
{
  for (auto l : listeners_)
    l->on_queenside_castling(color);
}

void
ListenerManager::on_player_check(const Color& color)
{
  for (auto l : listeners_)
    l->on_player_check(color);
}

void
ListenerManager::on_player_mat(const Color& color)
{
  for (auto l : listeners_)
    l->on_player_mat(color);
}

void
ListenerManager::on_player_pat(const Color& color)
{
  for (auto l : listeners_)
    l->on_player_pat(color);
}

void
ListenerManager::on_player_timeout(const Color& color)
{
  for (auto l : listeners_)
    l->on_player_timeout(color);
}

void
ListenerManager::on_player_disqualified(const Color& color)
{
  for (auto l : listeners_)
    l->on_player_disqualified(color);
}

void
ListenerManager::on_draw()
{
  for (auto l : listeners_)
    l->on_draw();
}

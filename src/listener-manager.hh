#ifndef LISTENER_MANAGER_HH
# define LISTENER_MANAGER_HH

# include <vector>
# include <string>
# include <iostream>
# include "include/listener.hh"

class ListenerManager
{
    private:
        ListenerManager();
        virtual ~ListenerManager();

    public:
       void error_(std::string message);

       static ListenerManager& get_instance();

       void load_listener(std::vector<std::string> listen_libs);
       std::vector<Listener*> listeners_get();

       /* FIXME */

       void manager_clean();

    public:
       void register_chessboard_interface(const ChessboardInterface&
                                          chessboard_interface);
       void on_game_started();
       void on_game_finished();
       void on_piece_moved(const PieceType& piece, const Position& from,
                          const Position& to);
       void on_piece_taken(const PieceType& piece, const Position& at);
       void on_piece_promoted(const PieceType& piece, const Position& p);
       void on_kingside_castling(const Color& color);
       void on_queenside_castling(const Color& color);
       void on_player_check(const Color& color);
       void on_player_mat(const Color& color);
       void on_player_pat(const Color& color);
       void on_player_timeout(const Color& color);
       void on_player_disqualified(const Color& color);
       void on_draw();

    protected:
       static ListenerManager listener_manager_;
       std::vector<Listener*> listeners_;
       std::vector<void*> opened_;
};

#endif /* !LISTENER_MANAGER_HH */

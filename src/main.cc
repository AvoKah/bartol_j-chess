#include "player/pgn-player.hh"
#include "include/color.hh"
#include "listener-manager.hh"
#include "chessgame-manager.hh"
#include "ai/my-ai.hh"
#include <iostream>
#include <cstring>

std::string get_ext(char *src, int length)
{
  int l = strlen(src);
  std::string ext = "";
  for (int i = l - length; i < l; ++i)
    ext += src[i];
  return ext;
}

int parse_input(int argc, char *argv[])
{
  std::string tmp = argv[1];
  if (argc == 2 && tmp == "ia")
      return 0;
  else if (argc < 3)
  {
    std::cerr << "chess: Error: At least two args are needed."
      << std::endl;
    return 1;
  }
  std::string ext = get_ext(argv[argc - 1], 4);
  if (ext == ".pgn")
  {
    for (int i = 1; i < argc - 1; i++)
    {
      if (get_ext(argv[i], 3) != ".so")
      {
        std::cerr << "chess: Error: Your library at the "
          << i << "e position is not available." << std::endl;
        return 1;
      }
    }
  }
  else
  {
    std::cerr << "chess: Error: Your last file is not a pgn file."
      << std::endl;
    return 1;
  }
  return 0;
}

int main(int argc, char *argv[])
{
    if (argc == 3 && (strcmp(argv[1], "--parse") == 0))
    {
        PgnParser pgnp = PgnParser(argv[2]);
        pgnp.parse();
        pgnp.dump();
        return 0;
    }
    else if (argc == 2 && (strcmp(argv[1], "ai") == 0))
    {
        Player* player1 = new MyAi(Color::WHITE);
        Player* player2 = new MyAi(Color::BLACK);
        ChessGameManager game = ChessGameManager(player1, player2);
        game.play();

        delete player1;
        delete player2;
        return 0;
    }

    if (!parse_input(argc, argv))
    {
        std::vector<std::string> l;
        for (int i = 1; i < argc - 1; i ++)
            l.push_back(argv[i]);

        ListenerManager::get_instance().load_listener(l);
        ListenerManager::get_instance().on_game_started();

        Player* player1 = new PgnPlayer(Color::WHITE, argv[argc - 1]);
        Player* player2 = new PgnPlayer(Color::BLACK, argv[argc - 1]);
        ChessGameManager game = ChessGameManager(player1, player2);
        game.play();

        ListenerManager::get_instance().on_game_finished();
        delete player1;
        delete player2;
        ListenerManager::get_instance().manager_clean();
    }
    else
        return 1;
}

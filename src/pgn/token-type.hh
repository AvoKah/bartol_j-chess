#ifndef TOKEN_TYPE_HH
# define TOKEN_TYPE_HH

enum class TokenType
{
    TAGNAME,
    TAGVALUE,
    LBRACKET,
    RBRACKET,
    MNI,
    SAN,
    EOG
};

#endif /* !TOKEN_TYPE_HH */

#ifndef TOKEN_HH
# define TOKEN_HH

# include <string>
# include "token-type.hh"

class Token
{
    public:
        Token(TokenType type, std::string val)
        : type_(type),
          val_(val)
        {}

        inline TokenType type_get();
        inline std::string val_get();
    private:
        TokenType type_;
        std::string val_;
};

# include "token.hxx"

#endif /* !TOKEN_HH */

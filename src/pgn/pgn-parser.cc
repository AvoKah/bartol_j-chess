#include <iostream>

#include "pgn-parser.hh"
#include "token-type.hh"

PgnParser::PgnParser(std::string filename)
    :lexer_(filename)
{
    lexer_.scan();
}

PgnParser::~PgnParser()
{}

void PgnParser::error_(std::string message)
{
    std::cerr << message << std::endl;
    exit(1);
}

void PgnParser::parse()
{
    parse_tag_section(lexer_.tag_section_get());
    parse_movetext_section(lexer_.movetext_section_get());
}

void PgnParser::parse_tag_section(std::vector<Token> tags)
{
    int i = 0;
    int length = (int) tags.size();

    while (i < length)
    {
       if (is_tag(tags, i))
       {
           if (tags[i + 1].val_get() == "White")
               add_name(Color::WHITE, tags[i + 2].val_get());
           else if (tags[i + 1].val_get() == "Black")
               add_name(Color::BLACK, tags[i + 2].val_get());
       }
       else
           error_("Invalid TAG " + tags[i + 1].val_get());
        i += 4;
    }
}

void PgnParser::parse_movetext_section(std::vector<Token> moves)
{
    int i = 0;
    int length = (int) moves.size();

    if (!check_Mni(moves))
        error_("Invalid move order");

    while (i < length)
    {
        if (is_move(moves, i) && (i != (length - 1)))
        {
            add_move(Color::WHITE, moves[i + 1].val_get());
            if ((i + 2) != (length - 1))
                add_move(Color::BLACK, moves[i + 2].val_get());
        }
        else if (i == (length - 1))
        {
            if (!(moves[i].type_get() == TokenType::EOG))
                error_("Invalid End of Game : " + moves[i].val_get());
            else
                break;
        }
        else
            error_("Invalid move sequence : " + moves[i].val_get());

        i += 3;
        if ((i - 1) == (length - 1))
            --i;
    }
}

bool PgnParser::is_tag(std::vector<Token> tags, int index)
{
    return (tags[index].type_get() == TokenType::LBRACKET
            && tags[index + 1].type_get() == TokenType::TAGNAME
            && tags[index + 2].type_get() == TokenType::TAGVALUE
            && tags[index + 3].type_get() == TokenType::RBRACKET);
}

bool PgnParser::is_move(std::vector<Token> moves, int index)
{
    if (moves[index].type_get() == TokenType::MNI
        && moves[index + 1].type_get() == TokenType::SAN)
    {
        if (moves[index + 2].type_get() == TokenType::SAN
                || moves[index + 2].type_get() == TokenType::EOG)
            return true;
    }

    return false;
}

bool PgnParser::check_Mni(std::vector<Token> moves)
{
    int old = 0;
    int length = (int) moves.size();
    for (int i = 0; i < length - 2; i += 3)
    {
        int tmp = stoi(moves[i].val_get());
        if ((tmp - 1) != old)
            return false;

        old = tmp;
    }
    return true;
}

void PgnParser::add_name(Color color, std::string name)
{
    names_[color] = name;
}

void PgnParser::add_move(Color color, std::string piece_move)
{
    piece_moves_[color].push_back(piece_move);
}

void PgnParser::dump()
{
    std::cerr << ">>>>>>>>>> LEXER <<<<<<<<<<" << std::endl << std::endl;
    lexer_.dump();
    std::cerr << ">>>>>>>>>> PARSER <<<<<<<<<<" << std::endl << std::endl;

    std::cerr << "WHITE :" << names_[Color::WHITE] << std::endl;
    for (auto i : piece_moves_[Color::WHITE])
        std::cerr << i << " | ";
    std::cerr << std::endl;

    std::cerr << "BLACK :" << names_[Color::BLACK] << std::endl;
    for (auto i : piece_moves_[Color::BLACK])
        std::cerr << i << " | ";
    std::cerr << std::endl;
}

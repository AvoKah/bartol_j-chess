#ifndef PGN_PARSER_HXX
# define PGN_PARSER_HXX

inline std::string PgnParser::name_get(Color color)
{
    return names_[color];
}

inline std::vector<std::string> PgnParser::piece_moves_get(Color color)
{
    return piece_moves_[color];
}

#endif /* !PGN_PARSER_HXX */

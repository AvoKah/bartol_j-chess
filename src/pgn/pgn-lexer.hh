#ifndef PGN_LEXER_HH
# define PGN_LEXER_HH

# include <string>
# include <vector>

# include "token.hh"

class PgnLexer
{
    public:
        PgnLexer(std::string filename);

        void scan();
        void scan_tag_section(std::string input);
        void scan_movetext_section(std::string input);

        int get_sequence(std::string input, int index, char end);
        bool check_san(std::string input);
        bool check_game_terminaison(std::string input);

        bool is_piece(char c);
        bool is_coordinate(std::string input, int index);
        bool is_promoted(std::string input, int index);
        bool is_special(std::string input, int index);
        bool is_valid(std::string input, int index);

        bool is_number(std::string str);

        void dump();

        inline std::vector<Token> tag_section_get();
        inline std::vector<Token> movetext_section_get();

    private:
        std::string filename_;
        std::vector<Token> tag_section_;
        std::vector<Token> movetext_section_;
};

# include "pgn-lexer.hxx"

#endif /* !PGN_LEXER_HH */

#ifndef PGN_PARSER_HH
# define PGN_PARSER_HH

# include <string>
# include <vector>
# include <map>

# include "../include/color.hh"
# include "../pieces/piece.hh"
# include "pgn-lexer.hh"
# include "token.hh"

class PgnParser
{
    public:
        PgnParser(std::string filename);
        ~PgnParser();

        void error_(std::string message);

        void parse();
        void parse_tag_section(std::vector<Token> tags);
        void parse_movetext_section(std::vector<Token> moves);

        bool is_tag(std::vector<Token> tags, int index);
        bool is_move(std::vector<Token> moves, int index);
        bool check_Mni(std::vector<Token> moves);

        void add_name(Color color, std::string name);
        void add_move(Color color, std::string piece_move);

        inline std::string name_get(Color color);
        inline std::vector<std::string> piece_moves_get(Color color);

        void dump();

    private:
        PgnLexer lexer_;
        std::map<Color, std::string> names_;
        std::map<Color, std::vector<std::string>> piece_moves_;
};

# include "pgn-parser.hxx"

#endif /* !PGN_PARSER_HH */

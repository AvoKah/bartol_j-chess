#ifndef PGN_LEXER_HXX
# define PGN_LEXER_HXX

# include "pgn-lexer.hh"

inline std::vector<Token> PgnLexer::tag_section_get()
{
    return tag_section_;
}

inline std::vector<Token> PgnLexer::movetext_section_get()
{
    return movetext_section_;
}

#endif /* !PGN_LEXER_HXX */

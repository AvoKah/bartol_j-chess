#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>

#include "pgn-lexer.hh"
#include "token-type.hh"
#include "../chess-utils.hh"

#define DEBUG

PgnLexer::PgnLexer(std::string filename)
: filename_(filename)
{}

/*=======================*/
/* SCAN FUNCTION         */
/*=======================*/

void PgnLexer::scan()
{
    std::string extension = filename_.substr(filename_.length() - 3, 3);
    if (extension != "pgn")
        error_("Invalid extension", 1);

    std::ifstream file_in(filename_);
    if (file_in)
    {
        bool head = false;
        std::string line;
        std::string content = "";
        while (getline(file_in, line))
        {
            if ((line == "") && !head && (content != ""))
            {
                PgnLexer::scan_tag_section(content);
                content = "";
                head = true;
            }

            content += line + "\n";
        }
        PgnLexer::scan_movetext_section(content);
        file_in.close();
    }
    else
        error_("Invalid file", 1);
}

void PgnLexer::scan_tag_section(std::string input)
{
    int length = (int)input.length();
    for (int i = 0; i < length; i++)
    {
        if (input[i] == '[') {
            tag_section_.push_back(Token(TokenType::LBRACKET, "["));
        }
        else if (input[i] == ']') {
            tag_section_.push_back(Token(TokenType::RBRACKET, "]"));
        }
        else if (std::isalpha(input[i])) {
            i = get_sequence(input, i, ' ');
        }
        else if (input[i] == '\"') {
            char c = 34;
            i = get_sequence(input, i + 1, c);
        }
    }
}

void PgnLexer::scan_movetext_section(std::string input)
{
    char *tmp = strdup(input.c_str());
    char *tok = std::strtok(tmp, ". \n");
    while (tok != NULL)
    {
        std::string tokstr = std::string(tok);
        if (is_number(tokstr))
        {
            movetext_section_.push_back(Token(TokenType::MNI, tokstr));
        }
        else if (check_game_terminaison(tokstr))
        {
            char *aux = std::strtok(NULL, " \n");
            if (aux != NULL)
                error_("Unvalid End of game", 1);
            else
            {
                movetext_section_.push_back(Token(TokenType::EOG, tok));
                break;
            }
        }
        else
        {
            if (tokstr.length() < 2)
                error_("Invalid SAN : too short : "
                                            + tokstr, 1);

            if (check_san(tokstr))
                movetext_section_.push_back(Token(TokenType::SAN, tokstr));
            else
                error_("Invalid SAN : bad format : "
                                            + tokstr, 1);
        }
        tok = std::strtok(NULL, ". \n");
    }
    free(tmp);
}

/*=======================*/
/* SUB FUNCTION          */
/*=======================*/

int PgnLexer::get_sequence(std::string input, int index, char end)
{
    std::string tok;
    int aux = index;
    int length = (int)input.length();
    while ((input[aux] != end) && (aux < length))
    {
        if (end == ' ' && !std::isalpha(input[aux]))
            error_("Invalid tag name : " +
                                        tok + input[aux], 1);

        tok += input[aux];
        aux++;
    }

    if (aux == length)
        error_("Unexpected EOF", 1);
    else
    {
        if (end == '\"')
        {
            tag_section_.push_back(Token(TokenType::TAGVALUE, tok));
        }
        else
            tag_section_.push_back(Token(TokenType::TAGNAME, tok));
    }

    return aux;
}

bool PgnLexer::check_san(std::string input)
{
    int index = 0;

    if (input[index] == 'O')
        return ((input == "O-O-O") || (input == "O-O"));
    else if (is_piece(input[index]))
    {
        index++;
        index = is_distinguished(input, index);
        if (input[index] == 'x')
            index++;

        if (is_coordinate(input, index))
        {
           index += 2;
           if (is_special(input, index))
                return true;

            return true;
        }
    }
    else // Pawn
        return is_valid(input, index);

    return false;
}

bool PgnLexer::check_game_terminaison(std::string input)
{
    return ((input == "1/2-1/2") || (input == "1-0") || (input == "0-1")
            || (input == "*"));
}

/*=======================*/
/* CHECK FUNCTION        */
/*=======================*/

bool PgnLexer::is_piece(char c)
{
    return (c == 'K') || (c == 'Q') || (c == 'B') || (c == 'N') || (c == 'R');
}

bool PgnLexer::is_coordinate(std::string input, int index)
{
    if ((index + 1) >= (int)input.length())
        return false;

    return (is_file(input[index])
            && is_rank(input[index + 1]));
}

bool PgnLexer::is_promoted(std::string input, int index)
{
    if ((index + 1) >= (int)input.length())
        return false;

    if ((input[index] == '=') && is_piece(input[index + 1])
            && (input[index + 1] != 'K' ))
        return true;
    else
       return is_special(input, index);
}

bool PgnLexer::is_special(std::string input, int index)
{
    if (index >= (int)input.length())
        return false;
    if ((input[index] == '+') || (input[index] == '#'))
        return true;
    else
    {
        std::string msg = "Invalid SAN :not in check/checkmate/promoted :";
        error_(msg + input, 1);
    }

    return false;
}

bool PgnLexer::is_valid(std::string input, int index)
{
    index = is_distinguished(input, index);
    if (input[index] == 'x')
        index++;

    if (is_coordinate(input, index))
    {
        index += 2;
        if (is_promoted(input, index))
        {
            index += 2;
            if (is_special(input, index))
                return true;
            return true;
        }
        else if (is_special(input, index))
            return true;

        return true;
    }
    return false;
}

bool PgnLexer::is_number(std::string str)
{
    int lgth = (int) str.length();
    for (int i = 0; i < lgth; i++)
        if (!std::isdigit(str[i]))
            return false;

    return true;
}

/*=============*/
/* DUMP        */
/*=============*/

void PgnLexer::dump()
{
    int i = 0;
    std::cout << "TAG SECTION :" << std::endl;

    for (Token t : tag_section_)
    {
        std::cout << "Token " << i << " : \"" << t.val_get()
                  << "\"" << std::endl;
        i++;
    }

    std::cout << std::endl << "MOVETEXT SECTION : " << std::endl;

    for (Token t : movetext_section_)
    {
        std::cout << "Token " << i << " : \"" << t.val_get()
                  << "\"" << std::endl;
        i++;
    }
}


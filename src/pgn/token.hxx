#ifndef TOKEN_HXX
# define TOKEN_HXX

# include "token.hh"

inline TokenType Token::type_get()
{
    return type_;
}

inline std::string Token::val_get()
{
    return val_;
}

#endif /* !TOKEN_HXX */

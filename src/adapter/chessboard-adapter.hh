#ifndef CHESSBOARD_ADAPTER_HH
# define CHESSBOARD_ADAPTER_HH

# include "../include/chessboard-interface.hh"
# include "../chessboard/chessboard.hh"
# include <utility>

class ChessboardAdapter: public ChessboardInterface
{
    public:
        ChessboardAdapter(ChessBoard& chessboard);
        virtual ~ChessboardAdapter();

        std::pair<const PieceType, const Color>
        operator[](const Position& position) const override;

    protected:
        ChessBoard& chessboard_;
};

#endif /* !CHESSBOARD_ADAPTER_HH */

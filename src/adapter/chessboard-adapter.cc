#include "chessboard-adapter.hh"

ChessboardAdapter::ChessboardAdapter(ChessBoard& chessboard)
    :chessboard_(chessboard)
{}

ChessboardAdapter::~ChessboardAdapter()
{}

std::pair<const PieceType, const Color>
ChessboardAdapter::operator[](const Position& position) const
{
    Piece* p = chessboard_.get_piece(position);
    const std::pair<const PieceType, const Color>
        out(p->getPieceType(), p->getColor());
    return out;
}

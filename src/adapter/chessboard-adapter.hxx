#ifndef CHESSBOARD_ADAPTER_HXX
# define CHESSBOARD_ADAPTER_HXX

std::pair<const PieceType, const Color>
operator[](const Position& position) const
{
    Piece* p = chessboard_.get_piece(position);
    const std::pair<const PieceType, const Color>
        out(p->getPieceType, p->getColor);
    return out;
}

#endif /* !CHESSBOARD_ADAPTER_HXX */

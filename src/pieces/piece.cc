#include <iostream>
#include "piece.hh"

Piece::Piece(PieceType piece, Color color, Position position)
  :piece_(piece),
   color_(color),
   position_(position)
{}

Piece::~Piece()
{}

void Piece::dump()
{
  for (auto i : this->get_Move())
    std::cout << i << std::endl;
}

int Piece::valid_cell(std::vector<Piece*> pieces, Position cell, Color c)
{
  Piece* tmp  = nullptr;
  for (auto i : pieces)
    if (i->getPosition() == cell)
      tmp = i;
  if (!tmp)
    return -1;
  else if (tmp->getColor() != c)
    return 0;
  return 1;
}

std::vector<Position> Piece::get_possible_Move()
{
  return possible_move_;
}

void Piece::set_possible_Move(std::vector<Position> possible_move)
{
  possible_move_ = possible_move;
}

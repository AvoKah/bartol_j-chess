#include "rook.hh"

Rook::Rook(Color color, Position position)
  : Piece(PieceType::ROOK, color, position)
{}

std::vector<Position> Rook::get_Move()
{
  std::vector<Position> vect;
  Position pos = this->getPosition();
  Position::File f = pos.file_get();
  Position::Rank r = pos.rank_get();
  for (int i = 1; i < 9; i++)
  {
    auto f_new = static_cast<Position::File>(i);
    if (f_new != f)
      vect.push_back(Position(f_new, r));
  }
  for (int i = 1; i < 9; i++)
  {
    auto r_new = static_cast<Position::Rank>(i);
    if (r_new != r)
      vect.push_back(Position(f, r_new));
  }
  return vect;
}

std::vector<Position> Rook::get_valid_Move(std::vector<Piece*> chessboard)
{
  std::vector<Position> vect;
  Position pos = this->getPosition();
  Position::File f = pos.file_get();
  Position::Rank r = pos.rank_get();
  for (int i = f + 1; i < 9; i++)
  {
    auto f_new = static_cast<Position::File>(i);
    if (f_new != f)
    {
      if (valid_cell(chessboard, Position(f_new, r), color_) == -1)
        vect.push_back(Position(f_new, r));
      else if (valid_cell(chessboard, Position(f_new, r), color_) == 0)
      {
        vect.push_back(Position(f_new, r));
        break;
      }
      else
        break;
    }
  }
  for (int i = f - 1; i > 0; i--)
  {
    auto f_new = static_cast<Position::File>(i);
    if (f_new != f)
    {
      if (valid_cell(chessboard, Position(f_new, r), color_) == -1)
        vect.push_back(Position(f_new, r));
      else if (valid_cell(chessboard, Position(f_new, r), color_) == 0)
      {
        vect.push_back(Position(f_new, r));
        break;
      }
      else
        break;
    }
  }
  for (int i = r + 1; i < 9; i++)
  {
    Position::Rank r_new;
    r_new = static_cast<Position::Rank>(i);
    if (r_new != r)
    {
      if (valid_cell(chessboard, Position(f, r_new), color_) == -1)
        vect.push_back(Position(f, r_new));
      else if (valid_cell(chessboard, Position(f, r_new), color_) == 0)
      {
        vect.push_back(Position(f, r_new));
        break;
      }
      else
        break;
    }
  }
  for (int i = r - 1; i > 0; i--)
  {
    Position::Rank r_new;
    r_new = static_cast<Position::Rank>(i);
    if (r_new != r)
    {
      if (valid_cell(chessboard, Position(f, r_new), color_) == -1)
        vect.push_back(Position(f, r_new));
      else if (valid_cell(chessboard, Position(f, r_new), color_) == 0)
      {
        vect.push_back(Position(f, r_new));
        break;
      }
      else
        break;
    }
  }
  possible_move_ = vect;
  return vect;
}

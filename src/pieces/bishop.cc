#include "bishop.hh"

Bishop::Bishop(Color color, Position position)
  : Piece(PieceType::BISHOP, color, position)
{}

std::vector<Position> Bishop::get_Move()
{
  std::vector<Position> vect;
  Position pos = this->getPosition();
  Position::File f = pos.file_get();
  Position::Rank r = pos.rank_get();
  while (f != Position::File::ANNA && r != Position::Rank::EINS)
  {
    --f;
    --r;
    vect.push_back(Position(f, r));
  }
  f = pos.file_get();
  r = pos.rank_get();
  while (f != Position::File::HECTOR && r != Position::Rank::EINS)
  {
    ++f;
    --r;
    vect.push_back(Position(f, r));
  }
  f = pos.file_get();
  r = pos.rank_get();
  while (f != Position::File::ANNA && r != Position::Rank::ACHT)
  {
    --f;
    ++r;
    vect.push_back(Position(f, r));
  }
  f = pos.file_get();
  r = pos.rank_get();
  while (f != Position::File::HECTOR && r != Position::Rank::ACHT)
  {
    ++f;
    ++r;
    vect.push_back(Position(f, r));
  }
  return vect;
}

std::vector<Position> Bishop::get_valid_Move(std::vector<Piece*> chessboard)
{
  std::vector<Position> vect;
  Position pos = this->getPosition();
  Position::File f = pos.file_get();
  Position::Rank r = pos.rank_get();

  while (f != Position::File::ANNA && r != Position::Rank::EINS)
  {
    --f;
    --r;

    if (valid_cell(chessboard, Position(f, r), color_) == -1)
      vect.push_back(Position(f, r));
    else if (valid_cell(chessboard, Position(f, r), color_) == 0)
    {
      vect.push_back(Position(f, r));
      break;
    }
    else
      break;
  }
  f = pos.file_get();
  r = pos.rank_get();
  while (f != Position::File::HECTOR && r != Position::Rank::EINS)
  {
    ++f;
    --r;
    if (valid_cell(chessboard, Position(f, r), color_) == -1)
      vect.push_back(Position(f, r));
    else if (valid_cell(chessboard, Position(f, r), color_) == 0)
    {
      vect.push_back(Position(f, r));
      break;
    }
    else
      break;
  }
  f = pos.file_get();
  r = pos.rank_get();
  while (f != Position::File::ANNA && r != Position::Rank::ACHT)
  {
    --f;
    ++r;
    if (valid_cell(chessboard, Position(f, r), color_) == -1)
      vect.push_back(Position(f, r));
    else if (valid_cell(chessboard, Position(f, r), color_) == 0)
    {
      vect.push_back(Position(f, r));
      break;
    }
    else
      break;
  }
  f = pos.file_get();
  r = pos.rank_get();
  while (f != Position::File::HECTOR && r != Position::Rank::ACHT)
  {
    ++f;
    ++r;
    if (valid_cell(chessboard, Position(f, r), color_) == -1)
      vect.push_back(Position(f, r));
    else if (valid_cell(chessboard, Position(f, r), color_) == 0)
    {
      vect.push_back(Position(f, r));
      break;
    }
    else
      break;
  }
  possible_move_ = vect;
  return vect;
}

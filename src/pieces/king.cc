#include "king.hh"

King::King(Color color, Position position)
  : Piece(PieceType::KING, color, position)
  , has_moved_(false)
{}

std::vector<Position> King::get_Move()
{
  std::vector<Position> vect;
  Position pos = this->getPosition();
  int f = pos.file_get();
  int r = pos.rank_get();
  for (int i = f - 1; i < f + 2; i++)
  {
    for (int j = r - 1; j < r + 2; j++)
    {
      if ((i == f && j == r) || i > 8 || j > 8 || i < 1 || j < 1)
      {}
      else
      {
        auto f = static_cast<Position::File>(i);
        auto r = static_cast<Position::Rank>(j);
        vect.push_back(Position(f, r));
      }
    }
  }
  return vect;
}

std::vector<Position> King::get_valid_Move(std::vector<Piece*> chessboard)
{
  std::vector<Position> vect;
  Position pos = this->getPosition();
  int f = pos.file_get();
  int r = pos.rank_get();
  for (int i = f - 1; i < f + 2; i++)
  {
    for (int j = r - 1; j < r + 2; j++)
    {
      if ((i == f && j == r) || i > 8 || j > 8 || i < 1 || j < 1)
      {}
      else
      {
        auto f = static_cast<Position::File>(i);
        auto r = static_cast<Position::Rank>(j);
        if (valid_cell(chessboard, Position(f, r), color_) < 1)
          vect.push_back(Position(f, r));
      }
    }
  }
  possible_move_ = vect;
  return vect;
}

void King::moved()
{
  has_moved_ = true;
}

bool King::has_moved()
{
  return has_moved_;
}

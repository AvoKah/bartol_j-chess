#ifndef BISHOP_HH_
# define BISHOP_HH_

# include "../include/piece-type.hh"
# include "../include/move.hh"
# include "piece.hh"

class Bishop: public Piece
{
  public:
    Bishop(Color color, Position position);
    std::vector<Position> get_Move() override;
    std::vector<Position> get_valid_Move(std::vector<Piece*> chessboard)
      override;
};

#endif /* !BISHOP_HH_ */

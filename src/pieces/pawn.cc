#include "pawn.hh"

Pawn::Pawn(Color color, Position position)
  : Piece(PieceType::PAWN, color, position)
{}

std::vector<Position> Pawn::get_Move()
{
  std::vector<Position> vect;
  Position pos = this->getPosition();
  Color color = this->getColor();
  if (color == Color::WHITE)
  {
    if (pos.rank_get() == Position::Rank::ZWEI)
    {
      auto f = static_cast<Position::File>(pos.file_get());
      auto r = static_cast<Position::Rank>(pos.rank_get() + 2);
      vect.push_back(Position(f, r));
    }
    if (pos.rank_get() != Position::Rank::ACHT)
    {
      auto f = static_cast<Position::File>(pos.file_get());
      auto r = static_cast<Position::Rank>(pos.rank_get() + 1);
      vect.push_back(Position(f, r));
    }
  }
  else
  {
    if (pos.rank_get() == Position::Rank::SIEBEN)
    {
      auto f = static_cast<Position::File>(pos.file_get());
      auto r = static_cast<Position::Rank>(pos.rank_get() - 2);
      vect.push_back(Position(f, r));
    }
    if (pos.rank_get() != Position::Rank::EINS)
    {
      auto f = static_cast<Position::File>(pos.file_get());
      auto r = static_cast<Position::Rank>(pos.rank_get() - 1);
      vect.push_back(Position(f, r));
    }
  }
  return vect;
}

std::vector<Position> Pawn::get_valid_Move(std::vector<Piece*> chessboard)
{
  std::vector<Position> vect;

  Position pos = this->getPosition();
  Color color = this->getColor();

  if (color == Color::WHITE)
  {
    if (pos.rank_get() == Position::Rank::ZWEI)
    {
      auto f = static_cast<Position::File>(pos.file_get());
      auto r = static_cast<Position::Rank>(pos.rank_get() + 2);

      /* START WITH THE TWO */
      if (valid_cell(chessboard, Position(f, r), color_) == -1)
        vect.push_back(Position(f, r));

      f = static_cast<Position::File>(pos.file_get() + 1);
      --r;

      /* CAN EAT */
      if (valid_cell(chessboard, Position(f, r), color_) == 0)
        vect.push_back(Position(f, r));

      f = static_cast<Position::File>(pos.file_get() - 1);
      if (valid_cell(chessboard, Position(f, r), color_) == 0)
        vect.push_back(Position(f, r));
    }

    if (pos.rank_get() != Position::Rank::ACHT)
    {
      auto f = static_cast<Position::File>(pos.file_get());
      auto r = static_cast<Position::Rank>(pos.rank_get() + 1);

      if (valid_cell(chessboard, Position(f, r), color_) == -1)
        vect.push_back(Position(f, r));

      f = static_cast<Position::File>(pos.file_get() + 1);
      if (valid_cell(chessboard, Position(f, r), color_) == 0)
        vect.push_back(Position(f, r));

      f = static_cast<Position::File>(pos.file_get() - 1);
      if (valid_cell(chessboard, Position(f, r), color_) == 0)
        vect.push_back(Position(f, r));
    }
  }
  else
  {
    if (pos.rank_get() == Position::Rank::SIEBEN)
    {
      auto f = static_cast<Position::File>(pos.file_get());
      auto r = static_cast<Position::Rank>(pos.rank_get() - 2);
      if (valid_cell(chessboard, Position(f, r), color_) == -1)
        vect.push_back(Position(f, r));
      f = static_cast<Position::File>(pos.file_get() + 1);
      ++r;
      if (valid_cell(chessboard, Position(f, r), color_) == 0)
        vect.push_back(Position(f, r));
      f = static_cast<Position::File>(pos.file_get() - 1);
      if (valid_cell(chessboard, Position(f, r), color_) == 0)
        vect.push_back(Position(f, r));
    }
    if (pos.rank_get() != Position::Rank::EINS)
    {
      auto f = static_cast<Position::File>(pos.file_get());
      auto r = static_cast<Position::Rank>(pos.rank_get() - 1);
      if (valid_cell(chessboard, Position(f, r), color_) == -1)
        vect.push_back(Position(f, r));
      f = static_cast<Position::File>(pos.file_get() + 1);
      if (valid_cell(chessboard, Position(f, r), color_) == 0)
        vect.push_back(Position(f, r));
      f = static_cast<Position::File>(pos.file_get() - 1);
      if (valid_cell(chessboard, Position(f, r), color_) == 0)
        vect.push_back(Position(f, r));
    }
  }
  possible_move_ = vect;
  return vect;
}

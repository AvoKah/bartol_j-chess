#include "knight.hh"

  Knight::Knight(Color color, Position position)
: Piece(PieceType::KNIGHT, color, position)
{}

std::vector<Position> Knight::get_Move()
{
  std::vector<Position> vect;
  Position pos = this->getPosition();
  Position::File f = pos.file_get();
  Position::Rank r = pos.rank_get();
  if (f > Position::File::ANNA && r > Position::Rank::ZWEI)
  {
    auto f_new = static_cast<Position::File>(f - 1);
    auto r_new = static_cast<Position::Rank>(r - 2);
    vect.push_back(Position(f_new, r_new));
  }
  if (f > Position::File::BELLA && r > Position::Rank::EINS)
  {
    auto f_new = static_cast<Position::File>(f - 2);
    auto r_new = static_cast<Position::Rank>(r - 1);
    vect.push_back(Position(f_new, r_new));
  }
  if (f > Position::File::BELLA && r < Position::Rank::ACHT)
  {
    auto f_new = static_cast<Position::File>(f - 2);
    auto r_new = static_cast<Position::Rank>(r + 1);
    vect.push_back(Position(f_new, r_new));
  }
  if (f > Position::File::ANNA && r < Position::Rank::SIEBEN)
  {
    auto f_new = static_cast<Position::File>(f - 1);
    auto r_new = static_cast<Position::Rank>(r + 2);
    vect.push_back(Position(f_new, r_new));
  }
  if (f < Position::File::HECTOR && r > Position::Rank::ZWEI)
  {
    auto f_new = static_cast<Position::File>(f + 1);
    auto r_new = static_cast<Position::Rank>(r - 2);
    vect.push_back(Position(f_new, r_new));
  }
  if (f < Position::File::GUSTAV && r > Position::Rank::EINS)
  {
    auto f_new = static_cast<Position::File>(f + 2);
    auto r_new = static_cast<Position::Rank>(r - 1);
    vect.push_back(Position(f_new, r_new));
  }
  if (f < Position::File::GUSTAV && r < Position::Rank::ACHT)
  {
    auto f_new = static_cast<Position::File>(f + 2);
    auto r_new = static_cast<Position::Rank>(r + 1);
    vect.push_back(Position(f_new, r_new));
  }
  if (f < Position::File::HECTOR && r < Position::Rank::SIEBEN)
  {
    auto f_new = static_cast<Position::File>(f + 1);
    auto r_new = static_cast<Position::Rank>(r + 2);
    vect.push_back(Position(f_new, r_new));
  }
  return vect;
}

std::vector<Position> Knight::get_valid_Move(std::vector<Piece*> chessboard)
{
  std::vector<Position> vect;
  Position pos = this->getPosition();
  Position::File f = pos.file_get();
  Position::Rank r = pos.rank_get();
  if (f > Position::File::ANNA && r > Position::Rank::ZWEI)
  {
    auto f_new = static_cast<Position::File>(f - 1);
    auto r_new = static_cast<Position::Rank>(r - 2);
    if (valid_cell(chessboard, Position(f_new, r_new), color_) < 1)
      vect.push_back(Position(f_new, r_new));
  }
  if (f > Position::File::BELLA && r > Position::Rank::EINS)
  {
    auto f_new = static_cast<Position::File>(f - 2);
    auto r_new = static_cast<Position::Rank>(r - 1);
    if (valid_cell(chessboard, Position(f_new, r_new), color_) < 1)
      vect.push_back(Position(f_new, r_new));
  }
  if (f > Position::File::BELLA && r < Position::Rank::ACHT)
  {
    auto f_new = static_cast<Position::File>(f - 2);
    auto r_new = static_cast<Position::Rank>(r + 1);
    if (valid_cell(chessboard, Position(f_new, r_new), color_) < 1)
      vect.push_back(Position(f_new, r_new));
  }
  if (f > Position::File::ANNA && r < Position::Rank::SIEBEN)
  {
    auto f_new = static_cast<Position::File>(f - 1);
    auto r_new = static_cast<Position::Rank>(r + 2);
    if (valid_cell(chessboard, Position(f_new, r_new), color_) < 1)
      vect.push_back(Position(f_new, r_new));
  }
  if (f < Position::File::HECTOR && r > Position::Rank::ZWEI)
  {
    auto f_new = static_cast<Position::File>(f + 1);
    auto r_new = static_cast<Position::Rank>(r - 2);
    if (valid_cell(chessboard, Position(f_new, r_new), color_) < 1)
      vect.push_back(Position(f_new, r_new));
  }
  if (f < Position::File::GUSTAV && r > Position::Rank::EINS)
  {
    auto f_new = static_cast<Position::File>(f + 2);
    auto r_new = static_cast<Position::Rank>(r - 1);
    if (valid_cell(chessboard, Position(f_new, r_new), color_) < 1)
      vect.push_back(Position(f_new, r_new));
  }
  if (f < Position::File::GUSTAV && r < Position::Rank::ACHT)
  {
    auto f_new = static_cast<Position::File>(f + 2);
    auto r_new = static_cast<Position::Rank>(r + 1);
    if (valid_cell(chessboard, Position(f_new, r_new), color_) < 1)
      vect.push_back(Position(f_new, r_new));
  }
  if (f < Position::File::HECTOR && r < Position::Rank::SIEBEN)
  {
    auto f_new = static_cast<Position::File>(f + 1);
    auto r_new = static_cast<Position::Rank>(r + 2);
    if (valid_cell(chessboard, Position(f_new, r_new), color_) < 1)
      vect.push_back(Position(f_new, r_new));
  }
  possible_move_ = vect;
  return vect;
}

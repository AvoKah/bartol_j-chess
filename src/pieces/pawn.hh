#ifndef PAWN_HH_
# define PAWN_HH_

# include "piece.hh"

class Pawn: public Piece
{
  public:
    Pawn(Color color, Position position);
    std::vector<Position> get_Move() override;
    std::vector<Position> get_valid_Move(std::vector<Piece*> chessboard)
      override;
};

#endif /* !PAWN_HH_ */

#ifndef PIECE_HH_
# define PIECE_HH_

# include <vector>

# include "../include/color.hh"
# include "../include/piece-type.hh"
# include "../include/position.hh"

/*
** \brief The main class. Each pieceType (Pawn, King...) will inherit
** of this one
*/
class Piece
{
  public:
    Piece(PieceType piece, Color color, Position position);
    virtual ~Piece();

  public:
    /*
    ** \brief function that will be overrided by each pieceTypes
    ** to get all possible moves that can do the fellow piece.
    **
    ** @return the list of all possible positions of the piece after a move.
    */
    virtual std::vector<Position> get_Move() = 0;
    virtual std::vector<Position>
      get_valid_Move(std::vector<Piece*> chessboard) = 0;
    std::vector<Position> get_possible_Move();
    void set_possible_Move(std::vector<Position> possible_move);
    int valid_cell(std::vector<Piece*> pieces, Position cell, Color c);
    void dump();

  public:
    PieceType getPieceType();
    Color getColor();
    Position getPosition();

  public:
    Piece* setPosition(Position position);

  protected:
    std::vector<Position> possible_move_;
    PieceType piece_;
    Color color_;
    Position position_;

};

inline std::ostream& operator<<(std::ostream& ostr, Piece& piece);

# include "piece.hxx"

#endif /* !PIECE_HH_ */

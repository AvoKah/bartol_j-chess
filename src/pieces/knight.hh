#ifndef KNIGHT_HH_
# define KNIGHT_HH_

# include "../include/piece-type.hh"
# include "../include/move.hh"
# include "piece.hh"

class Knight: public Piece
{
  public:
    Knight(Color color, Position position);
    std::vector<Position> get_Move() override;
    std::vector<Position> get_valid_Move(std::vector<Piece*> chessboard)
      override;
};

#endif /* !KNIGHT_HH_ */

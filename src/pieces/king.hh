#ifndef KING_HH_
# define KING_HH_

# include "../include/piece-type.hh"
# include "../include/move.hh"
# include "piece.hh"

class King: public Piece
{
  public:
    King(Color color, Position position);
    std::vector<Position> get_Move() override;
    std::vector<Position> get_valid_Move(std::vector<Piece*> chessboard)
      override;

  public:
    void moved();
    bool has_moved();

  private:
    bool has_moved_;
};

#endif /* !KING_HH_ */

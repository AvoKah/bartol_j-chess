#ifndef ROOK_HH_
# define ROOK_HH_

# include "../include/piece-type.hh"
# include "../include/move.hh"
# include "piece.hh"

class Rook: public Piece
{
  public:
    Rook(Color color, Position position);
    std::vector<Position> get_Move() override;
    std::vector<Position> get_valid_Move(std::vector<Piece*> chessboard)
      override;
};

#endif /* !ROOK_HH_ */

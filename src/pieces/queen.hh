#ifndef QUEEN_HH_
# define QUEEN_HH_

# include "../include/piece-type.hh"
# include "../include/move.hh"
# include "piece.hh"

class Queen: public Piece
{
  public:
    Queen(Color color, Position position);
    std::vector<Position> get_Move() override;
    std::vector<Position> get_valid_Move(std::vector<Piece*> chessboard)
      override;
};

#endif /* !QUEEN_HH_ */

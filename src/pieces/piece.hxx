#ifndef PIECE_HXX_
# define PIECE_HXX_

inline PieceType Piece::getPieceType()
{
  return this->piece_;
}

inline Color Piece::getColor()
{
  return this->color_;
}

inline Position Piece::getPosition()
{
  return this->position_;
}

inline Piece* Piece::setPosition(Position position)
{
  this->position_ = position;
  return this;
}

inline std::ostream& operator<<(std::ostream& ostr, Piece& piece)
{
  char c;
  switch (piece.getPieceType()){
    case PieceType::KING:
      c = 'K';
      break;
    case PieceType::QUEEN:
      c = 'Q';
      break;
    case PieceType::ROOK:
      c = 'R';
      break;
    case PieceType::BISHOP:
      c = 'B';
      break;
    case PieceType::KNIGHT:
      c = 'N';
      break;
    case PieceType::PAWN:
      c = 'P';
      break;
    default:
      c = ' ';
      break;
  }

  if (piece.getColor() == Color::WHITE)
    return ostr << c;
  else
    return ostr << "\033[91m" << c << "\033[0m";
}

#endif /* !PIECE_HXX_ */

#ifndef CHESS_GAME_MANAGER_
# define CHESS_GAME_MANAGER_

# include "include/listener.hh"
# include "include/player.hh"
# include "chessboard/chessboard.hh"

class ChessGameManager
{
  public:
    ChessGameManager(Player* p1, Player* p2);
    ~ChessGameManager();

  public:
    void play();

  protected:
    Player* player1_;
    Player* player2_;
    ChessBoard chessboard_;
};

#endif /* !CHESS_GAME_MANAGER_ */

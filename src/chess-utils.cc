#include <iostream>
#include <algorithm>
#include "chess-utils.hh"
#include "pieces/bishop.hh"
#include "pieces/king.hh"
#include "pieces/knight.hh"
#include "pieces/pawn.hh"
#include "pieces/queen.hh"
#include "pieces/rook.hh"

/*===========*/
/* METHODS   */
/*===========*/

void error_(std::string message, int exit_code)
{
    std::cerr << message << std::endl;
    exit(exit_code);
}

bool is_file(char c)
{
    return ((c >= 'a') && (c <= 'h'));
}

int get_file(char c)
{
    return c - 96;
}

bool is_rank(char c)
{
    return ((c >= '1') && (c <= '8'));
}

int is_distinguished(std::string input, int index)
{
    if (is_file(input[index])
        && (input[index + 1] == 'x' || is_file(input[index + 1])))
        return index + 1;
    else if (is_rank(input[index])
        && (input[index + 1] == 'x' || is_file(input[index + 1])))
        return index + 1;
    else if (is_file(input[index]) && is_rank(input[index + 1])
             && (input[index + 1] == 'x' || is_file(input[index + 1])))
        return index + 2;
    else
        return index;
}

bool is_piece(char c)
{
    return (c == 'K') || (c == 'Q') || (c == 'B') || (c == 'N') || (c == 'R');
}

bool is_promoted(std::string input, int index)
{
    if ((index + 1) >= (int)input.length())
        return false;

    if ((input[index] == '=') && is_piece(input[index + 1])
            && (input[index + 1] != 'K' ))
        return true;

    return false;
}

Piece* get_piece(Position cell, std::vector<Piece*> chessboard)
{
  for (auto i : chessboard)
    if (i->getPosition() == cell)
      return i;
  return nullptr;
}

Piece* get_piece(PieceType type, Color color, std::vector<Piece*> chessboard)
{
  for (auto i : chessboard)
    if ((i->getPieceType() == type) && (i->getColor() == color))
      return i;
  return nullptr;
}

PieceType get_type(Position cell, std::vector<Piece*> pieces)
{
  for (auto i : pieces)
    if (i->getPosition() == cell)
      return i->getPieceType();
  return PieceType::NONE;
}

Position white(Position::File p, bool is_pawn)
{
  if (is_pawn)
    return Position(p, Position::ZWEI);
  return Position(p, Position::EINS);
}

Position black(Position::File p, bool is_pawn)
{
  if (is_pawn)
    return Position(p, Position::SIEBEN);
  return Position(p, Position::ACHT);
}

void init(std::vector<Piece*>& pieces)
{
    ChessBoard chessboard_ = ChessBoard();
    chessboard_.init();
    pieces = chessboard_.get_pieces();
}

void make_move(Move move, std::vector<Piece*>& pieces)
{
    ChessBoard chessboard_ = ChessBoard();
    chessboard_.pieces_set(pieces);
    chessboard_.make_move(move);
    pieces = chessboard_.get_pieces();
}

Piece* create_piece(PieceType type, Color c, Position p)
{
  switch (type)
  {
    case PieceType::BISHOP:
      return new Bishop(c, p);
      break;
    case PieceType::KING:
      return new King(c, p);
      break;
    case PieceType::KNIGHT:
      return new Knight(c, p);
      break;
    case PieceType::PAWN:
      return new Pawn(c, p);
      break;
    case PieceType::QUEEN:
      return new Queen(c, p);
      break;
    case PieceType::ROOK:
      return new Rook(c, p);
      break;
    default:
      return nullptr;
      break;
  }
}

bool checkmate(ChessBoard chessboard_, Color color)
{
  Color ennemies = (color == Color::WHITE)? Color::BLACK : Color::WHITE;
  Piece* kpiece = chessboard_.get_piece(
          chessboard_.get_king(color).getPosition());


  if (!chessboard_.pos_threatened(kpiece->getPosition(), ennemies))
      return false;

  std::vector<Position> vect; /* = kpiece->get_possible_Move();*/
  for (auto possible : kpiece->get_valid_Move(chessboard_.get_pieces()))
  {
      if (!chessboard_.pos_threatened(possible, ennemies))
          vect.push_back(possible);
  }

  if (vect.size() == 0)
      return false;

  std::vector<Position> threat;
  for (auto i : vect)
  {
    if (!chessboard_.pos_threatened(i, ennemies))
        return false;
    else
        threat.push_back(i);
  }

  auto mypieces = chessboard_.get_color_pieces(color);
  auto threats = chessboard_.piece_threatened(kpiece->getPosition(), ennemies);

  for (auto t : threats)
      for (auto p : mypieces)
          if (p->getPieceType() != PieceType::KING)
          {
              for (auto pos : p->get_valid_Move(chessboard_.get_pieces()))
              {
                  if (between(kpiece->getPosition(), t->getPosition(), pos))
                  {
                      return false;
                  }
              }
          }

  return true;
}

bool between(Position start, Position end, Position bet)
{
    int br = bet.rank_get();
    int sr = start.rank_get();
    int er = end.rank_get();

    int bf = bet.file_get();
    int sf = start.file_get();
    int ef = end.file_get();

    if ((sr == br) && (sr == er))
        return ((sf <= bf) && (bf <= ef)) || ((sf >= bf) && (bf >= ef));
    else if ((sf == bf) && (sf == ef))
        return ((sr <= br) && (br <= er)) || ((sr >= br) && (br >= er));
    else if (((sr < br) && (br < er)) || ((sr > br) && (br > er)))
        return ((sf < bf) && (bf < ef)) || ((sf > bf) && (bf > ef));

    return false;
}

int checkstate(ChessBoard chessboard_, Color color)
{
    Color ennemies = (color == Color::WHITE)? Color::BLACK : Color::WHITE;

    Piece* k = get_piece(PieceType::KING, color, chessboard_.get_pieces());

    if (!k)
        return 2;

    if (checkmate(chessboard_, color))
        return 1;

    if (chessboard_.pos_threatened(k->getPosition(), ennemies))
        return 0;

    return -1;
}

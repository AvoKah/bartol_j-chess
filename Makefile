CXX=g++
CXXFLAGS=-Wextra -Wall -Werror -std=c++1y -pedantic
DEL= *.o *~ .*.swp *.so *.class *.log *.a
SRC=ai/my-ai.cc include/ai.cc include/xmlinterface.cc include/color.hh \
	include/position.cc include/move.cc include/piece-type.hh pieces/piece.cc \
	pieces/king.cc pieces/bishop.cc pieces/queen.cc pieces/rook.cc \
	pieces/knight.cc pieces/pawn.cc chess-utils.cc include/player.cc \
	chessboard/chessboard.cc

SRC:=$(addprefix src/,$(SRC))
BIN=chess

all:
	make -C _build/
	mv _build/chess ./

ai:
	$(CXX) -std=c++14 -shared -fPIC -o libai.so $(SRC)

coding_style:tests/coding_style.py
	./tests/coding_style.py src/*
	./tests/coding_style.py src/*/*

clean:
	rm -rf $(DEL) $(BIN) $(OBJ) pgn _build
